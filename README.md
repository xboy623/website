FakeBaseball Website
---

Welcome! Thanks for checking out the source code of the /r/fakebaseball website and API.
Below is a guide on how to get the project running locally.

### What you'll need:
* An installation of JDK 8 ([Windows](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html), [Linux](https://openjdk.java.net/install/))
* A MySQL 8 schema to test on (If you don't already have one running, you can ask Llamos#0001 to host one for you)
* An SBT installation ([Windows, Linux](https://www.scala-sbt.org/download.html))
* A Reddit web app ([Reddit Console](https://www.reddit.com/prefs/apps/))
* An installation of FlywayDB ([Windows, Linux](https://flywaydb.org/documentation/commandline/#download-and-installation))

### Set-up
1. Make sure you have everything listed in "What you'll need"
2. Fork and clone the project
3. Create `conf/development.conf` with the following contents: 
```toml
include "application.conf"

slick.dbs.default.db {
  url = "jdbc:mysql://YOUR_DATABASE_URL/YOUR_DATABAE_SCHEMA"
  user = "YOUR_DATABASE_USERNAME"
  password = "YOUR_DATABASE_PASSWORD"
}

reddit {
  clientId = "YOUR_REDDIT_APP_CLIENT_ID"
  clientSecret = "YOUR_REDDIT_APP_CLIENT_SECRET"
}
```

Finally, run the project with `sbt run`.
Database migrations should be applied automatically.

### Project Overview

This project uses the Play! Framework which has extensive documentation [here](https://www.playframework.com/documentation/2.7.x/Home).
Play! is an MVC-style framework, similar to ASP.NET, but using Scala.
Please read the documentation there if you would like to contribute to the backend of the site.

Source code files are listed under `app/`, separated into the following packages:
* `controllers`: Contains the web controllers which respond to web requests.
* `model`: Represents the structure of data within the project.
* `services`: Contains services which interact with external components (e.g. Reddit)
* `views`: The HTML files shown to the end user. Written as [Twirl Templates](https://www.playframework.com/documentation/2.7.x/ScalaTemplates).

By using Twirl templates, other views can be included in pages.
For example, the navigation bar can be written in one file,
and then included in all subsequent views.
Please keep this style in mind when writing new views.

### Migrations

The database is kept up to date by using [FlywayDB's database migration system](https://flywaydb.org/documentation/migrations).
Migrations are stored in `conf/evolutions/ups` and `conf/evolutions/downs`.
Migrations stored in `ups` should be prefixed by `Vxy`,
and implement the forward-migration of a change to the database.
Migrations stored in `downs` should be prefixed by `Uxy`,
and implement the backward-migration of a respective change in `ups`.
For both, `x` is the date of creation in `YYYYMMDD` format
and `y` is the time of creation in `HHMMSS` format.
Each migration is written in pure SQL.

### Discord

If you haven't yet joined the Discord server to discuss this site's development,
[join here](https://discord.gg/QeNJ72C)!
