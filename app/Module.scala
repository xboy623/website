import com.google.inject.AbstractModule
import model.FBDatabase
import services.RedditService

class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[RedditService])
    bind(classOf[FBDatabase])
  }

}
