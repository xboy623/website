package model

import play.api.data.Forms._
import play.api.data._

case class UmpireAssignmentForm(umpire: String, game: Int, addition: Boolean)

object UmpireAssignmentForm {

  def umpireAssignmentForm(implicit db: FBDatabase): Form[UmpireAssignmentForm] = Form(
    mapping(
      "umpire" -> nonEmptyText.verifying("This user does not exist or is not an umpire.", db.getUserByRedditName(_).exists(_.isUmpire)),
      "game" -> number(min = 1).verifying("This game does not exist.", db.getGameById(_).isDefined),
      "addition" -> boolean
    )(UmpireAssignmentForm.apply)(UmpireAssignmentForm.unapply)
      .verifying("This umpire is already assigned to this game.", ua => !ua.addition || !db.isUmpireAssignedToGame(db.getUserByRedditName(ua.umpire).get, ua.game))
  )

}
