package model

import play.api.data._
import play.api.data.Forms._

// This whole form could use cleaning up
case class LineupForm(force: Boolean, player1: Int, p1Pos: String, player2: Int, p2Pos: String, player3: Int, p3Pos: String, player4: Int, p4Pos: String, player5: Int, p5Pos: String, player6: Int, p6Pos: String, player7: Int, p7Pos: String, player8: Int, p8Pos: String, player9: Int, p9Pos: String, pitcher: Int) {
  def tupled: Seq[(Int, String)] = Seq((player1, p1Pos), (player2, p2Pos), (player3, p3Pos), (player4, p4Pos), (player5, p5Pos), (player6, p6Pos), (player7, p7Pos), (player8, p8Pos), (player9, p9Pos))
}

object LineupForm {

  def lineupForm(team: Team)(implicit db: FBDatabase) = Form(
    mapping(
      "force" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "player1" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p1Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player2" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p2Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player3" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p3Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player4" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p4Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player5" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p5Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player6" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p6Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player7" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p7Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player8" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p8Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "player9" -> number(min = 1).verifying("This is not a valid player for this team.", db.getPlayerById(_).exists(_.team.exists(_ == team.id))),
      "p9Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
      "pitcher" -> number(min = 1).verifying("This is not a valid pitcher for this team.", db.getPlayerById(_).exists(p => p.team.contains(team.id) && (team.milr || p.positionPrimary == "P"))),
    )(LineupForm.apply)(LineupForm.unapply)
      .verifying("This lineup contains the same player twice.", lineup => lineup.force || lineup.tupled.map(_._1).toSet.size == 9)
      .verifying("This lineup contains the same position twice.", lineup => lineup.force || lineup.tupled.map(_._2).toSet.size == 9)
      .verifying("This lineup contains a pitcher-batter and a DH.", lineup => lineup.force || lineup.tupled.map(_._2).count(pos => pos == "DH" || pos == "P") == 1)
      .verifying("The pitcher-batter must be the pitcher.", lineup => lineup.force || lineup.tupled.find(_._2 == "P").forall(_._1 == lineup.pitcher))
      .verifying("The player in position 1 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p1Pos == "DH" || db.getPlayerById(lineup.player1).exists(player => player.positionPrimary == lineup.p1Pos || player.positionSecondary.contains(lineup.p1Pos) || player.positionTertiary.contains(lineup.p1Pos)))
      .verifying("The player in position 2 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p2Pos == "DH" || db.getPlayerById(lineup.player2).exists(player => player.positionPrimary == lineup.p2Pos || player.positionSecondary.contains(lineup.p2Pos) || player.positionTertiary.contains(lineup.p2Pos)))
      .verifying("The player in position 3 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p3Pos == "DH" || db.getPlayerById(lineup.player3).exists(player => player.positionPrimary == lineup.p3Pos || player.positionSecondary.contains(lineup.p3Pos) || player.positionTertiary.contains(lineup.p3Pos)))
      .verifying("The player in position 4 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p4Pos == "DH" || db.getPlayerById(lineup.player4).exists(player => player.positionPrimary == lineup.p4Pos || player.positionSecondary.contains(lineup.p4Pos) || player.positionTertiary.contains(lineup.p4Pos)))
      .verifying("The player in position 5 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p5Pos == "DH" || db.getPlayerById(lineup.player5).exists(player => player.positionPrimary == lineup.p5Pos || player.positionSecondary.contains(lineup.p5Pos) || player.positionTertiary.contains(lineup.p5Pos)))
      .verifying("The player in position 6 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p6Pos == "DH" || db.getPlayerById(lineup.player6).exists(player => player.positionPrimary == lineup.p6Pos || player.positionSecondary.contains(lineup.p6Pos) || player.positionTertiary.contains(lineup.p6Pos)))
      .verifying("The player in position 7 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p7Pos == "DH" || db.getPlayerById(lineup.player7).exists(player => player.positionPrimary == lineup.p7Pos || player.positionSecondary.contains(lineup.p7Pos) || player.positionTertiary.contains(lineup.p7Pos)))
      .verifying("The player in position 8 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p8Pos == "DH" || db.getPlayerById(lineup.player8).exists(player => player.positionPrimary == lineup.p8Pos || player.positionSecondary.contains(lineup.p8Pos) || player.positionTertiary.contains(lineup.p8Pos)))
      .verifying("The player in position 9 is ineligible to field this position.", lineup => lineup.force || team.milr || lineup.p9Pos == "DH" || db.getPlayerById(lineup.player9).exists(player => player.positionPrimary == lineup.p9Pos || player.positionSecondary.contains(lineup.p9Pos) || player.positionTertiary.contains(lineup.p9Pos)))
  )

}
