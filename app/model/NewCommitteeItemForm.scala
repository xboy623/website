package model

import play.api.data._
import play.api.data.Forms._

case class NewCommitteeItemForm(itemId: Int, title: String, body: String, isPrivate: Boolean)

object NewCommitteeItemForm {

  val newCommitteeItemForm = Form(
    mapping(
      "itemId" -> number(min = 0),
      "title" -> nonEmptyText(minLength = 15, maxLength = 200),
      "body" -> nonEmptyText(minLength = 100, maxLength = 40000),
      "isPrivate" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_))
    )(NewCommitteeItemForm.apply)(NewCommitteeItemForm.unapply)
  )

}
