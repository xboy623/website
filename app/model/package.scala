import services.StatCalculatorService

import scala.language.implicitConversions

package object model {

  val CURRENT_SEASON: Int = 3

  val availablePositions: Seq[String] = Seq(
    "P",
    "C",
    "1B",
    "2B",
    "3B",
    "SS",
    "LF",
    "CF",
    "RF"
  )

  val lineupPositions: Set[String] = availablePositions.toSet + "DH"

  val PLAY_TYPE_SWING: Int = 1
  val PLAY_TYPE_AUTO_K: Int = 2
  val PLAY_TYPE_AUTO_BB: Int = 3
  val PLAY_TYPE_BUNT: Int = 4
  val PLAY_TYPE_INFIELD_IN: Int = 5
  val PLAY_TYPE_STEAL: Int = 6
  val PLAY_TYPE_MULTI_STEAL: Int = 7
  val PLAY_TYPE_IBB: Int = 8

  val PLAY_TYPES: Map[String, Int] = Map(
    "Swing" -> PLAY_TYPE_SWING,
    "Auto K" -> PLAY_TYPE_AUTO_K,
    "Auto BB" -> PLAY_TYPE_AUTO_BB,
    "Bunt" -> PLAY_TYPE_BUNT,
    "Infield In" -> PLAY_TYPE_INFIELD_IN,
    "Steal" -> PLAY_TYPE_STEAL,
    "Multi-Steal" -> PLAY_TYPE_MULTI_STEAL,
    "IBB" -> PLAY_TYPE_IBB,
  )

  import Tables._

  // Table mappings to better names
  type BattingStatSet = BattingStatsRow
  type BattingType = BattingTypesRow
  type CommitteeItem = CommitteeItemsRow
  type CommitteeItemMotioner = CommitteeItemMotionersRow
  type CommitteeItemVote = CommitteeItemVotesRow
  type GameAction = GameActionsRow
  type Game = GamesRow
  type GMAssignment = GmAssignmentsRow
  type Lineup = LineupsRow
  type LineupEntry = LineupEntriesRow
  type Park = ParksRow
  type PitchingBonus = PitchingBonusesRow
  type PitchingStatSet = PitchingStatsRow
  type PitchingType = PitchingTypesRow
  type PlayerApplication = PlayerApplicationsRow
  type Player = PlayersRow
  type Team = TeamsRow
  type UmpireAssignment = UmpireAssignmentsRow
  type User = UsersRow

  implicit def battingType2Range(bt: BattingType): CalcRange = CalcRange(bt.rangeHr, bt.range3b, bt.range2b, bt.range1b, bt.rangeBb, bt.rangeFo, bt.rangeK, bt.rangePo, bt.rangeRgo, bt.rangeLgo)
  implicit def pitchingType2Range(pt: PitchingType): CalcRange = CalcRange(pt.rangeHr, pt.range3b, pt.range2b, pt.range1b, pt.rangeBb, pt.rangeFo, pt.rangeK, pt.rangePo, pt.rangeRgo, pt.rangeLgo)
  implicit def pitchingBonus2Range(pb: PitchingBonus): CalcRange = CalcRange(pb.rangeHr, pb.range3b, pb.range2b, pb.range1b, pb.rangeBb, pb.rangeFo, pb.rangeK, pb.rangePo, pb.rangeRgo, pb.rangeLgo)
  case class SwingResult(diff: Int, result: String)

  implicit class GameActionExtensions(recentPlay: GameAction) {
    def preview: String = {
      (
        recentPlay.playType match {
          case PLAY_TYPE_BUNT => "Bunt"
          case PLAY_TYPE_AUTO_K => "Auto K"
          case PLAY_TYPE_AUTO_BB => "Auto BB"
          case PLAY_TYPE_STEAL | PLAY_TYPE_MULTI_STEAL => "Steal"
          case _ => "Swing"
        }
      ) + s": ${recentPlay.swing.map(_.toString).getOrElse("x")}  \nPitch: ${recentPlay.pitch.map(_.toString).getOrElse("x")}  \n${recentPlay.diff.map(_.toString).getOrElse("x")} -> ${recentPlay.result.getOrElse("Unknown")}"
    }
  }

  implicit class BattingStatsExtensions(stats: BattingStatSet) {
    def totalH: Int = StatCalculatorService.calculateHits(stats)
    def ba: Float = StatCalculatorService.calculateBA(stats)
    def obp: Float = StatCalculatorService.calculateOBP(stats)
    def slg: Float = StatCalculatorService.calculateSLG(stats)
    def ops: Float = StatCalculatorService.calculateOPS(stats)
  }

  implicit class PitchingStatsExtensions(stats: PitchingStatSet) {
    def totalIP: Float = StatCalculatorService.calculateIP(stats)
    def era: Float = StatCalculatorService.calculateERA(stats)
    def whip: Float = StatCalculatorService.calculateWHIP(stats)
    def totalGo: Int = StatCalculatorService.calculateGO(stats)
  }

  // Common combo types
  type UserWithPlayerTuple = (User, Option[Player])
  case class UserWithPlayer(user: User, player: Option[Player])
  def tuple2UserWithPlayer(tuple: UserWithPlayerTuple): UserWithPlayer = UserWithPlayer(tuple._1, tuple._2)

  type PlayerWithTypesTuple = (Player, BattingType, Option[PitchingType], Option[PitchingBonus])
  case class PlayerWithTypes(player: Player, battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus])
  def tuple2PlayerWithTypes(tuple: PlayerWithTypesTuple): PlayerWithTypes = PlayerWithTypes(tuple._1, tuple._2, tuple._3, tuple._4)

  type GameWithTeamsTuple = (Game, Team, Team)
  case class GameWithTeams(game: Game, awayTeam: Team, homeTeam: Team) {
    def name = s"${game.season}-${game.session} ${awayTeam.tag}@${homeTeam.tag}"
  }
  def tuple2GameWithTeams(tuple: GameWithTeamsTuple): GameWithTeams = GameWithTeams(tuple._1, tuple._2, tuple._3)

  type GameWithRecentPlayTuple = (Game, Team, Team, Option[GameAction])
  case class GameWithRecentPlay(game: Game, awayTeam: Team, homeTeam: Team, recentPlay: Option[GameAction]){
    def name = s"${game.season}-${game.session} ${awayTeam.tag}@${homeTeam.tag}"
    def awayBatting: Boolean = recentPlay.map(_.resultInning).getOrElse(1) % 2 == 1
    def fieldingLineup: Option[Int] = if (awayBatting) game.homeLineup else game.awayLineup
    def battingLineup: Option[Int] = if (awayBatting) game.awayLineup else game.homeLineup
    def nextBatterLineupPos: Int = if (awayBatting) recentPlay.map(_.resultAwayBattingPosition).getOrElse(1) else recentPlay.map(_.resultHomeBattingPosition).getOrElse(1)
    def inning: String = {
      val i = recentPlay.map(_.resultInning).getOrElse(1)
      if(i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}"
    }
    def inningWithOuts: String = if (game.completed) "FINAL" else s"$inning ${recentPlay.map(_.resultOuts).getOrElse(0)} Out"
  }
  def tuple2GameWithRecentPlay(tuple: GameWithRecentPlayTuple): GameWithRecentPlay = GameWithRecentPlay(tuple._1, tuple._2, tuple._3, tuple._4)

  type PlayerWithStatsTuple = (Player, User, BattingStatSet, PitchingStatSet)
  case class PlayerWithStats(player: Player, user: User, battingStats: BattingStatSet, pitchingStats: PitchingStatSet)
  def tuple2PlayerWithStats(tuple: PlayerWithStatsTuple): PlayerWithStats = PlayerWithStats(tuple._1, tuple._2, tuple._3, tuple._4)

  type LineupEntryWithPlayerTuple = (LineupEntry, PlayerWithStatsTuple)
  case class LineupEntryWithPlayer(lineupEntry: LineupEntry, player: PlayerWithStats)
  def tuple2LineupWithPlayers(tuple: LineupEntryWithPlayerTuple): LineupEntryWithPlayer = LineupEntryWithPlayer(tuple._1, tuple2PlayerWithStats(tuple._2))

  type CommitteeItemWithAuthorTuple = (CommitteeItem, User, String)
  case class CommitteeItemWithAuthor(item: CommitteeItem, user: User, author: String)
  def tuple2CommitteeItemWithAuthor(tuple: CommitteeItemWithAuthorTuple): CommitteeItemWithAuthor = CommitteeItemWithAuthor(tuple._1, tuple._2, tuple._3)

  type CommitteeItemMotionerWithUserTuple = (CommitteeItemMotioner, User, String)
  case class CommitteeMotionerWithUser(user: User, name: String)
  def tuple2CommitteeMotionerWithUser(tuple: CommitteeItemMotionerWithUserTuple): CommitteeMotionerWithUser = CommitteeMotionerWithUser(tuple._2, tuple._3)

  type GameActionWithPlayersTuple = (GameAction, Option[Player], Option[Player])
  case class GameActionWithPlayers(gameAction: GameAction, batter: Option[Player], pitcher: Option[Player])
  def tuple2GameActionWithPlayers(tuple: GameActionWithPlayersTuple): GameActionWithPlayers = GameActionWithPlayers(tuple._1, tuple._2, tuple._3)

}
