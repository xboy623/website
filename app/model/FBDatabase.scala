package model

import java.sql.{Date, Timestamp}
import java.util.Calendar
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.Tables._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.MySQLProfile
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.language.implicitConversions

@Singleton
class FBDatabase @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[MySQLProfile] {

  private val UserWithPlayerQuery = Users
    .joinLeft(Players).on(_.id === _.user)
  private val PlayerWithTypesQuery = Players
    .join(BattingTypes).on(_.battingType === _.id)
    .joinLeft(PitchingTypes).on(_._1.pitchingType === _.id)
    .joinLeft(PitchingBonuses).on(_._1._1.pitchingBonus === _.id)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))
  private val GameWithTeamsQuery = Games
    .join(Teams).on(_.awayTeam === _.id)
    .join(Teams).on(_._1.homeTeam === _.id)
    .map(all => (all._1._1, all._1._2, all._2))
  private val GameWithTeamsAndRecentPlayQuery = GameWithTeamsQuery
    .joinLeft(GameActions.filter(_.replacedBy.isEmpty)).on(_._1.id === _.gameId)
    .map(all => (all._1._1, all._1._2, all._1._3, all._2))
  private val PlayerWithStatsQuery = Players
    .join(Users).on(_.user === _.id)
    .join(BattingStats).on(_._1.id === _.player)
    .join(PitchingStats).on(_._1._1.id === _.player)
    .map(all => (all._1._1._1, all._1._1._2, all._1._2, all._2))
  private val LineupEntriesWithPlayers = LineupEntries
    .join(PlayerWithStatsQuery).on(_.player === _._1.id)
    .map(all => (all._1, all._2))
  private val CommitteeItemsWithAuthorsQuery = CommitteeItems
    .join(UserWithPlayerQuery).on(_.author === _._1.id)
    .map(all => (all._1, all._2._1, all._2._2.map(_.name).getOrElse(all._2._1.redditName)))
  private val CommitteeItemMotionersWithNamesQuery = CommitteeItemMotioners
    .join(UserWithPlayerQuery).on(_.voter === _._1.id)
    .map(all => (all._1, all._2._1, all._2._2.map(_.name).getOrElse(all._2._1.redditName)))
  private val GameActionsWithPlayersQuery = GameActions
    .joinLeft(Players).on(_.batter === _.id)
    .joinLeft(Players).on(_._1.pitcher === _.id)
    .map(all => (all._1._1, all._1._2, all._2))

  private def awaitDB[R](awaitable: DBIOAction[R, NoStream, Nothing]): R = {
    Await.result(db.run(awaitable), Duration(5, TimeUnit.SECONDS))
  }

  def getUserByRedditName(reddit: String): Option[User] = awaitDB {
    Users.filter(_.redditName === reddit).take(1).result.headOption
  }

  def getPlayerByRedditName(reddit: String): Option[Player] = awaitDB {
    (for {
      u <- Users if u.redditName === reddit
      p <- Players if p.user === u.id
    } yield p).take(1).result.headOption
  }

  def getPlayerByName(name: String): Option[Player] = awaitDB {
    Players.filter(_.name === name).take(1).result.headOption
  }

  def getPlayerById(id: Int): Option[Player] = awaitDB {
    Players.filter(_.id === id).take(1).result.headOption
  }

  def getPlayerWithStatsById(id: Int): Option[PlayerWithStats] = awaitDB {
    PlayerWithStatsQuery
      .filter(_._1.id === id)
      .take(1)
      .result
      .headOption
  }.map(tuple2PlayerWithStats)

  def getBattingTypeByShortcode(code: String): Option[BattingType] = awaitDB {
    BattingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingTypeByShortcode(code: String): Option[PitchingType] = awaitDB {
    PitchingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingBonusByShortcode(code: String): Option[PitchingBonus] = awaitDB {
    PitchingBonuses.filter(_.shortcode === code).take(1).result.headOption
  }

  def getBattingTypeById(id: Int): BattingType = awaitDB {
    BattingTypes.filter(_.id === id).take(1).result.head
  }

  def getPitchingTypeById(id: Int): PitchingType = awaitDB {
    PitchingTypes.filter(_.id === id).take(1).result.head
  }

  def getPitchingBonusById(id: Int): PitchingBonus = awaitDB {
    PitchingBonuses.filter(_.id === id).take(1).result.head
  }

  def createPlayerApplication(application: NewPlayerForm)(implicit user: User): PlayerApplication = awaitDB {
    val bt = getBattingTypeByShortcode(application.battingType).get.id
    val pt = application.pitchingType.map(pt => getPitchingTypeByShortcode(pt).get.id)
    val pb = application.pitchingBonus.map(pb => getPitchingBonusByShortcode(pb).get.id)
    PlayerApplications returning PlayerApplications.map(_.id) into ((app, newId) => app.copy(id = newId)) += PlayerApplicationsRow(0, user.id, application.requestedName, application.returning, application.willingToJoinDiscord, application.positionPrimary, application.positionSecondary, application.rightHanded, bt, pt, pb, 0)
  }

  def getPlayerApplicationByUser(user: User): Option[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.user === user.id).take(1).result.headOption
  }

  def getPlayerApplicationById(id: Int): Option[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.id === id).take(1).result.headOption
  }

  def updatePlayerApplicationWithResponse(id: Int, newStatus: Int, rejectionMessage: Option[String], user: Int): Unit = awaitDB {
    PlayerApplications.filter(_.id === id)
      .map(app => (app.status, app.respondedAt, app.respondedBy, app.rejectMessage))
      .update((newStatus, Some(new Timestamp(System.currentTimeMillis())), Some(user), rejectionMessage))
  }

  def createNewPlayer(app: PlayerApplication): Player = {
    val newPlayer = awaitDB(Players returning Players.map(_.id) into ((player, newId) => player.copy(id = newId)) += PlayersRow(0, app.user, app.requestedName, None, app.battingType, app.pitchingType, app.pitchingBonus, app.isRightHanded, app.positionPrimary, app.positionSecondary, None))
    awaitDB(PitchingStats.map(_.player) += newPlayer.id)
    awaitDB(BattingStats.map(_.player) += newPlayer.id)
    newPlayer
  }

  def editPlayerApplication(appid: Int, application: NewPlayerForm): Int = awaitDB {
    val bt = getBattingTypeByShortcode(application.battingType).get.id
    val pt = application.pitchingType.map(pt => getPitchingTypeByShortcode(pt).get.id)
    val pb = application.pitchingBonus.map(pb => getPitchingBonusByShortcode(pb).get.id)
    PlayerApplications.filter(_.id === appid)
      .map(app => (app.status, app.requestedName, app.isReturningPlayer, app.isWillingToJoinDiscord, app.positionPrimary, app.positionSecondary, app.isRightHanded, app.battingType, app.pitchingType, app.pitchingBonus))
      .update((0, application.requestedName, application.returning, application.willingToJoinDiscord, application.positionPrimary, application.positionSecondary, application.rightHanded, bt, pt, pb))
  }

  def getMLRTeams: Seq[Team] = awaitDB {
    Teams.filterNot(_.milr).sortBy(_.name).result
  }

  def getMiLRTeams: Seq[Team] = awaitDB {
    Teams.filter(_.milr).sortBy(_.name).result
  }

  def getTeam(tag: String): Option[Team] = awaitDB {
    Teams.filter(_.tag === tag).take(1).result.headOption
  }

  def getPlayersOnTeam(team: Team): Seq[Player] = awaitDB {
    if (team.milr)
      (for {
        t <- Teams if t.milrTeam === team.id
        p <- Players if p.team === t.id
      } yield p).result
    else
      Players.filter(_.team === team.id).result
  }

  /**
    * Should only be used when the ID is expected to return a user.
    * Not to be used for arbitrary user/player lookup.
    *
    * @param id The ID of the user.
    * @return The User instance
    */
  def getUserById(id: Int): User = awaitDB {
    Users.filter(u => u.id === id).take(1).result.head
  }

  def createUserAccount(redditName: String): Int = awaitDB {
    Users returning Users.map(_.id) += UsersRow(0, redditName)
  }

  def assignGMToTeam(team: Team, newGM: Player, oldGMEndReason: Option[String]): Unit = awaitDB {
    // Swap out GM by ending old one and starting new one
    GmAssignments.filter(_.team === team.id)
      .filter(_.endStamp.isEmpty)
      .map(t => (t.endStamp, t.endReason))
      .update((Some(new Timestamp(System.currentTimeMillis())), oldGMEndReason)) andThen
      (GmAssignments += GmAssignmentsRow(0, team.id, newGM.id, new Timestamp(System.currentTimeMillis())))
  }

  def getGMOfTeam(team: Team): Option[Player] = awaitDB {
    (for {
      assignment <- GmAssignments if assignment.team === team.id && assignment.endStamp.isEmpty
      p <- Players if assignment.gm === p.id
    } yield p).result.headOption
  }

  def countPendingPlayerApplications(): Int = awaitDB {
    PlayerApplications.filter(_.status === 0).length.result
  }

  def getAllPendingApplications: Seq[PlayerApplication] = awaitDB {
    PlayerApplications.filter(_.status === 0).result
  }

  def getGameByTeamTag(season: Int, session: Int, team: String): Option[GamesRow] = awaitDB {
    (for {
      t <- Teams if t.tag === team
      g <- Games if g.season === season && g.session === session && (g.homeTeam === t.id || g.awayTeam === t.id)
    } yield g).take(1).result.headOption
  }

  def getTeamByTag(tag: String): Option[Team] = awaitDB {
    Teams.filter(_.tag === tag).take(1).result.headOption
  }

  def createGame(form: NewGameForm): Game = awaitDB {
    val awayTeamId = getTeamByTag(form.awayTeam).get.id
    val homeTeamId = getTeamByTag(form.homeTeam).get.id
    Games returning Games.map(_.id) into ((game, newId) => game.copy(id = newId)) += GamesRow(0, form.season, form.session, awayTeamId, homeTeamId, form.id36, gameStart = new java.sql.Date(form.startDate.getTime))
  }

  def markGameComplete(gameId: Int): Unit = awaitDB {
    Games
      .filter(_.id === gameId)
      .map(_.completed)
      .update(true)
  }

  def getAllActiveUmpires: Seq[UserWithPlayer] = awaitDB {
    (for {
      (u, p) <- Users.filter(_.isUmpire)
        .joinLeft(Players).on(_.id === _.user)
    } yield (u, p)).result
  }.map(tuple2UserWithPlayer)

  def setUmpireStatus(userId: Int, isUmpire: Boolean): Unit = awaitDB {
    Users.filter(_.id === userId)
      .map(_.isUmpire)
      .update(isUmpire)
  }

  def getGamesWithUmpAssignments: Seq[(GameWithTeams, Option[UserWithPlayer])] = awaitDB {
    GameWithTeamsQuery.filterNot(_._1.completed)
      .joinLeft(UmpireAssignments.join(UserWithPlayerQuery).on(_.umpire === _._1.id)).on(_._1.id === _._1.game)
      .map(all => (all._1, all._2.map(_._2))).result
  }.map(all => (tuple2GameWithTeams(all._1), all._2.map(tuple2UserWithPlayer)))

  def isUmpireAssignedToGame(umpire: User, gameId: Int): Boolean = awaitDB {
    UmpireAssignments.filter(ua => ua.umpire === umpire.id && ua.game === gameId).result.headOption
  }.isDefined

  def assignUmpireToGame(umpire: User, gameId: Int): Unit = awaitDB {
    UmpireAssignments += UmpireAssignmentsRow(gameId, umpire.id)
  }

  def removeUmpireFromGame(umpire: User, gameId: Int): Unit = awaitDB {
    UmpireAssignments.filter(ua => ua.umpire === umpire.id && ua.game === gameId).delete
  }

  def getGameById(id: Int): Option[Game] = awaitDB {
    Games.filter(_.id === id).take(1).result.headOption
  }

  def getGamesForUmpire(umpire: User): Seq[GameWithRecentPlay] = awaitDB {
    UmpireAssignments
      .filter(_.umpire === umpire.id)
      .join(GameWithTeamsAndRecentPlayQuery).on(_.game === _._1.id)
      .map(all => all._2)
      .result
  }.map(tuple2GameWithRecentPlay)

  def getGamesInSession(season: Int, session: Int): Seq[GameWithRecentPlay] = awaitDB {
    GameWithTeamsAndRecentPlayQuery
      .filter(g => g._1.season === season && g._1.session === session)
      .result
  }.map(tuple2GameWithRecentPlay)

  def getActiveGames: Seq[GameWithRecentPlay] = awaitDB {
    GameWithTeamsAndRecentPlayQuery
      .filter(g => !g._1.completed && g._4.isDefined)
      .result
  }.map(tuple2GameWithRecentPlay)

  def getUnstartedGames: Seq[GameWithTeams] = awaitDB {
    GameWithTeamsQuery
      .filterNot(_._1.completed)
      .result
  }.map(tuple2GameWithTeams)

  def getUpcomingGames: Seq[GameWithRecentPlay] = awaitDB {
    val minDate = Calendar.getInstance()
    minDate.add(Calendar.DATE, -2)
    val maxDate = Calendar.getInstance()
    maxDate.add(Calendar.DATE, 10)
    GameWithTeamsAndRecentPlayQuery
      .filter(g => !g._1.completed && g._4.isEmpty && g._1.gameStart > new Date(minDate.getTimeInMillis) && g._1.gameStart < new Date(maxDate.getTimeInMillis))
      .result
  }.map(tuple2GameWithRecentPlay)

  def getLineupWithPlayers(lineupId: Int): Seq[LineupEntryWithPlayer] = awaitDB {
    LineupEntriesWithPlayers
      .filter(_._1.lineup === lineupId)
      .sortBy(_._1.battingPos)
      .result
  }.map(tuple2LineupWithPlayers)

  def getGameWithInfoById(gameId: Int): Option[GameWithRecentPlay] = awaitDB {
    GameWithTeamsAndRecentPlayQuery
      .filter(_._1.id === gameId)
      .take(1)
      .result
      .headOption
  }.map(tuple2GameWithRecentPlay)

  def createLineup: Lineup = awaitDB {
    Lineups returning Lineups.map(_.id) into ((lineup, newId) => lineup.copy(id = newId)) += LineupsRow(0)
  }

  def setLineup(game: Game, lineup: Lineup, awayTeam: Boolean): Unit = awaitDB {
    Games.filter(_.id === game.id)
      .map(g => if (awayTeam) g.awayLineup else g.homeLineup)
      .update(Some(lineup.id))
  }

  def addLineupEntry(lineup: Int, player: Int, position: String, battingPos: Int, replaces: Option[Int] = None): Unit = {
    val newId = awaitDB {
      LineupEntries returning LineupEntries.map(_.id) += LineupEntriesRow(0, lineup, player, position, battingPos, None, new Timestamp(System.currentTimeMillis()))
    }
    replaces.map(replaces => awaitDB {
      LineupEntries.filter(_.id === replaces).map(_.replacedBy).update(Some(newId))
    })
  }

  def addGameAction(newAction: GameAction): Unit = awaitDB {
    val newId = awaitDB {
      GameActions returning GameActions.map(_.id) += newAction
    }
    newAction.batter.foreach(recalculateBattingStatsForPlayer)
    newAction.pitcher.foreach(recalculatePitchingStatsForPlayer)
    GameActions.filter(action => action.gameId === newAction.gameId && action.replacedBy.isEmpty && action.id =!= newId).map(_.replacedBy).update(Some(newId))
  }

  def recalculateBattingStatsForPlayer(player: Int): Unit = awaitDB {
    sqlu"""
          update batting_stats t
          set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and play_type != 6 and play_type != 7),
              total_abs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != '' and result != 'BB'),
              total_rbi=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != 'BB'),
              total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = 'HR'),
              total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = '3B'),
              total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = '2B'),
              total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = '1B'),
              total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and (result = 'BB' or result = 'Auto BB' or result = 'IBB')),
              total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = 'FO'),
              total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and (result = 'K' or result = 'Auto K' or result = 'Bunt K')),
              total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = 'PO'),
              total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = 'RGO'),
              total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result = 'LGO'),
              total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
              total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home')),
              total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and outs_tracked=2),
              total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and outs_tracked=3),
              total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null),
              total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result='FO' and runs_scored=1)
              where player=$player
      """
  }

  def recalculatePitchingStatsForPlayer(player: Int): Unit = awaitDB {
    sqlu"""
          update pitching_stats t
          set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and play_type != 6 and play_type != 7),
              total_outs=(select (case when sum(outs_tracked) is null then 0 else sum(outs_tracked) end) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and play_type != 3 and result != ''),
              total_er=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and play_type != 3 and result != 'BB'),
              total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'HR'),
              total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = '3B'),
              total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = '2B'),
              total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = '1B'),
              total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and (result = 'BB' or result = 'Auto BB' or result = 'IBB')),
              total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'FO'),
              total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'K'),
              total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'PO'),
              total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'RGO'),
              total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and result = 'LGO'),
              total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
              total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and pitcher = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home')),
              total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and outs_tracked=2),
              total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and outs_tracked=3),
              total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null),
              total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.season = $CURRENT_SEASON and (select id from teams where id=b.away_team and not milr) is not null) is not null and batter = t.player and result='FO' and runs_scored=1)
              where player=$player
        """
  }

  def initGame(game: Int): GameAction = awaitDB {
    GameActions returning GameActions.map(_.id) into ((action, newId) => action.copy(id = newId)) += new GameAction(0, None, game, 0, None, None, None, None, None, None, 1, 1, None, None, None, 0, 0, 0, 1, 0, 0)
  }

  def getPlayerWithTypes(playerId: Int): PlayerWithTypes = tuple2PlayerWithTypes(
    awaitDB {
      PlayerWithTypesQuery
        .filter(_._1.id === playerId)
        .take(1)
        .result
        .head
    }
  )

  def getParkForTeam(team: Team): Park = awaitDB {
    Parks
      .filter(_.id === team.park)
      .take(1)
      .result
      .head
  }

  def getVotingCommitteeItems: Seq[CommitteeItem] = awaitDB {
    CommitteeItems
      .filter(_.status === 1)
      .result
  }

  def getPublicCommitteeItems: Seq[CommitteeItem] = awaitDB {
    CommitteeItems
      .filter(item => item.status === 0 && !item.isPrivate)
      .result
  }

  def getAllCommitteeItems: Seq[CommitteeItem] = awaitDB {
    CommitteeItems
      .filter(_.status === 0)
      .result
  }

  def createNewCommitteeItem(form: NewCommitteeItemForm, user: User): Int = awaitDB {
    CommitteeItems returning CommitteeItems.map(_.id) += new CommitteeItem(0, form.title, form.body, form.isPrivate, user.id, 0)
  }

  def updateCommitteeItem(form: NewCommitteeItemForm, user: User): Unit = awaitDB {
    CommitteeItems
      .filter(ci => ci.id === form.itemId && ci.status =!= 1)
      .map(ci => (ci.title, ci.body, ci.isPrivate))
      .update((form.title, form.body, form.isPrivate))
  }

  def deleteCommitteeItem(item: CommitteeItem): Int = awaitDB {
    CommitteeItems
      .filter(_.id === item.id)
      .delete
  }

  def getCommitteeItemById(id: Int): Option[CommitteeItemWithAuthor] = awaitDB {
    CommitteeItemsWithAuthorsQuery
      .filter(_._1.id === id)
      .take(1)
      .result
      .headOption
  }.map(tuple2CommitteeItemWithAuthor)

  def getCommitteeItemMotioners(item: CommitteeItem): Seq[CommitteeMotionerWithUser] = awaitDB {
    CommitteeItemMotionersWithNamesQuery
      .filter(_._1.committeeItem === item.id)
      .result
  }.map(tuple2CommitteeMotionerWithUser)

  def getCommitteeItemVotes(item: CommitteeItem): Seq[CommitteeItemVote] = awaitDB {
    CommitteeItemVotes
      .filter(_.committeeItem === item.id)
      .result
  }

  def addMotionerToItem(item: CommitteeItem, user: User): Unit = {
    if (awaitDB(CommitteeItemMotioners.filter(motioner => motioner.committeeItem === item.id && motioner.voter === user.id).result.headOption).isEmpty)
      awaitDB(CommitteeItemMotioners += new CommitteeItemMotioner(item.id, user.id))
  }

  def removeMotionerFromItem(item: CommitteeItem, user: User): Unit = awaitDB {
    CommitteeItemMotioners.filter(motioner => motioner.committeeItem === item.id && motioner.voter === user.id).delete
  }

  def addVoteToItem(item: CommitteeItem, user: User, votedFor: Boolean): Unit = awaitDB {
    CommitteeItemVotes.insertOrUpdate(new CommitteeItemVote(item.id, user.id, votedFor))
  }

  def updateCommitteeItemStatus(item: CommitteeItem, state: Int): Unit = awaitDB {
    CommitteeItems
      .filter(_.id === item.id)
      .map(_.status)
      .update(state)
  }

  def getPlayerForUser(user: User): Option[Player] = awaitDB {
    UserWithPlayerQuery
      .filter(_._1.id === user.id)
      .map(_._2)
      .take(1)
      .result
      .headOption
  }.flatten

  def getGameLog(game: Game): Seq[GameActionWithPlayers] = awaitDB {
    GameActionsWithPlayersQuery
      .filter(_._1.gameId === game.id)
      .sortBy(_._1.id.desc)
      .result
  }.map(tuple2GameActionWithPlayers)

  def getGameActionWithPlayers(actionId: Int): Option[GameActionWithPlayers] = awaitDB {
    GameActionsWithPlayersQuery
      .filter(_._1.id === actionId)
      .take(1)
      .result
      .headOption
  }.map(tuple2GameActionWithPlayers)

  def deleteGameAction(actionId: Int): Unit = {
    awaitDB {
      GameActions
        .filter(_.replacedBy === actionId)
        .map(_.replacedBy)
        .update(None)
    }
    awaitDB {
      GameActions
        .filter(_.id === actionId)
        .delete
    }
  }

}
