package model

import play.api.data.Forms._
import play.api.data._

case class PlayerApplicationResponseForm(approval: Boolean, rejectionMessage: Option[String]) {

  def validateRejectionMessage(): Boolean = approval || rejectionMessage.isDefined

}

object PlayerApplicationResponseForm {

  val playerApplicationResponseForm: Form[PlayerApplicationResponseForm] = Form(
    mapping(
      "approval" -> boolean,
      "rejectionMsg" -> optional(nonEmptyText(1))
    )(PlayerApplicationResponseForm.apply)(PlayerApplicationResponseForm.unapply)
      .verifying("You must include a rejection message when rejecting an application.", _.validateRejectionMessage())
  )

}
