package model

case class CalcRange(var rangeHR: Int, var range3B: Int, var range2B: Int, var range1B: Int, var rangeBB: Int, var rangeFO: Int, var rangeK: Int, var rangePO: Int, var rangeRGO: Int, var rangeLGO: Int) {
  def +(other: CalcRange) = CalcRange(
    rangeHR + other.rangeHR,
    range3B + other.range3B,
    range2B + other.range2B,
    range1B + other.range1B,
    rangeBB + other.rangeBB,
    rangeFO + other.rangeFO,
    rangeK + other.rangeK,
    rangePO + other.rangePO,
    rangeRGO + other.rangeRGO,
    rangeLGO + other.rangeLGO,
  )
  def absolute: CalcRange = {
    var c = 0
    CalcRange(
      {c += rangeHR; c},
      {c += range3B; c},
      {c += range2B; c},
      {c += range1B; c},
      {c += rangeBB; c},
      {c += rangeFO; c},
      {c += rangeK; c},
      {c += rangePO; c},
      {c += rangeRGO; c},
      {c += rangeLGO; c},
    )
  }
}
