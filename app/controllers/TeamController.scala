package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class TeamController @Inject()(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def listTeams: Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.teams.list(db.getMLRTeams, db.getMiLRTeams)(req, user))
  }

  def viewTeam(tag: String): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getTeam(tag).map { team =>
      Ok(views.html.teams.view(team, db.getPlayersOnTeam(team), db.getGMOfTeam(team))(req, user))
    } getOrElse {
      NotFound("That team does not exist.")
    }
  }

}
