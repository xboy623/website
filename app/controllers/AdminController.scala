package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.EndGameForm._
import model.NewGameForm.newGameForm
import model.UmpireAssignmentForm.umpireAssignmentForm
import model.UmpireStatusForm.umpireStatusForm
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class AdminController @Inject()(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def adminPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.index(db.countPendingPlayerApplications()))
  }

  def listPendingApplications: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.applications(db.getAllPendingApplications))
  }

  def showNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    Ok(views.html.admin.newgame(teams, newGameForm, mlr))
  }

  def submitNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    newGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.newgame(teams, formWithErrors, mlr)),
      formData => {
        db.createGame(formData)
        Redirect(routes.AdminController.showNewGameForm(mlr))
      }
    )
  }

  def showUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.manageumpires(db.getAllActiveUmpires, umpireStatusForm))
  }

  def submitUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireStatusForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.manageumpires(db.getAllActiveUmpires, formWithErrors)),
      formData => {
        db.setUmpireStatus(db.getUserByRedditName(formData.user).get.id, formData.newUmpStatus)
        Redirect(routes.AdminController.showUmpireStatusForm())
      }
    )
  }

  def showUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnstartedGames, db.getGamesWithUmpAssignments, umpireAssignmentForm))
  }

  def submitUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireAssignmentForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnstartedGames, db.getGamesWithUmpAssignments, formWithErrors)),
      formData => {
        val ump = db.getUserByRedditName(formData.umpire).get
        if (formData.addition)
          db.assignUmpireToGame(ump, formData.game)
        else
          db.removeUmpireFromGame(ump, formData.game)
        Redirect(routes.AdminController.showUmpireAssignmentForm())
      }
    )
  }

  def viewEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.endgame(db.getActiveGames, endGameForm))
  }

  def submitEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    endGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.endgame(db.getActiveGames, formWithErrors)),
      formData => {
        db.markGameComplete(formData.gameId)
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

}
