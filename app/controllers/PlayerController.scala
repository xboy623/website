package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class PlayerController @Inject()(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def myPlayer: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.getPlayerForUser(ru).map { player =>
      Redirect(routes.PlayerController.showPlayer(player.id))
    } getOrElse {
      db.getPlayerApplicationByUser(ru).map { app =>
        Redirect(routes.PlayerApplicationController.showApplication(app.id))
      } getOrElse {
        Redirect(routes.PlayerApplicationController.showNewPlayerForm())
      }
    }
  }

  def showPlayer(id: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getPlayerWithStatsById(id).map { player =>
      Ok(views.html.players.view(player)(req, user))
    } getOrElse {
      NotFound("This player does not exist.")
    }
  }

}
