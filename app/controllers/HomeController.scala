package controllers

import javax.inject.{Inject, Singleton}
import model.{FBDatabase, Tables}
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class HomeController @Inject()(db: FBDatabase, messagesActionBuilder: MessagesActionBuilder,  cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    implicit val iUser: Option[Tables.UsersRow] = user
    Ok(views.html.index())
  }

}
