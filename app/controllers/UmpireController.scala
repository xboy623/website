package controllers

import javax.inject.{Inject, Singleton}
import model._
import model.LineupForm._
import model.NewGameActionForm._
import model.DeleteActionForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{MLRCalculatorService, RedditFormattingService}

@Singleton
class UmpireController @Inject()(calc: MLRCalculatorService)(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def index: Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    val games = db.getGamesForUmpire(ru)
    Ok(views.html.umpires.index(games))
  }

  private def generateBatterPing(game: GameWithRecentPlay, batter: LineupEntryWithPlayer, allPlays: Seq[GameActionWithPlayers]): String = {
    var hits = 0
    var abs = 0
    var plays = Seq[String]()
    var last: Option[GameActionWithPlayers] = None
    allPlays.reverse.foreach { play =>
      if (play.gameAction.batter.contains(batter.player.player.id)) {
        if (play.gameAction.result.contains("HR") | play.gameAction.result.contains("3B") | play.gameAction.result.contains("2B") | play.gameAction.result.contains("1B")) {
          val inning = last.map(_.gameAction.resultInning).map(i => if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}").getOrElse("T1")
          hits += 1
          abs += 1
          plays :+= s"${play.gameAction.result.get} in $inning"
        } else if (play.gameAction.result.contains("FO") | play.gameAction.result.contains("K") | play.gameAction.result.contains("PO") | play.gameAction.result.contains("RGO") | play.gameAction.result.contains("LGO")) {
          val inning = last.map(_.gameAction.resultInning).map(i => if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}").getOrElse("T1")
          abs += 1
          plays :+= s"${play.gameAction.result.get} in $inning"
        }
      }
      last = Some(play)
    }

    s"${game.awayTeam.tag} ${game.recentPlay.map(_.resultScoreAway).getOrElse(0)} - ${game.recentPlay.map(_.resultScoreHome).getOrElse(0)} ${game.homeTeam.tag} |" +
      s" ${if (game.recentPlay.exists(_.resultPlayerOnThird.isDefined)) "\u25C6" else "\u25C7"} ^${if (game.recentPlay.exists(_.resultPlayerOnSecond.isDefined)) "\u25C6" else "\u25C7"} ${if (game.recentPlay.exists(_.resultPlayerOnFirst.isDefined)) "\u25C6" else "\u25C7"} |" +
      s" ${game.recentPlay.map(_.resultInning).map(i => if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}").getOrElse("T1")} ${game.recentPlay.map(_.resultOuts).getOrElse(0)} Out\n\n" +
      s"${if (game.awayBatting) game.awayTeam.tag else game.homeTeam.tag} ${batter.lineupEntry.position} [${batter.player.player.name}](${batter.player.user.redditName})" +
      s": $hits-$abs: ${plays.mkString(", ")}"
  }

  def showNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      game.fieldingLineup.map(db.getLineupWithPlayers).flatMap { fieldingLineup =>
        game.battingLineup.map(db.getLineupWithPlayers).map { battingLineup =>

          // Get the current batter and pitcher to show on form
          val nextBatter = battingLineup.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty).get
          val pitcher = fieldingLineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get

          // Generate the batter ping
          val batterPing = generateBatterPing(game, nextBatter, db.getGameLog(game.game))

          // Display form
          val potentialStealers = Seq(game.recentPlay.flatMap(_.resultPlayerOnFirst), game.recentPlay.flatMap(_.resultPlayerOnSecond), game.recentPlay.flatMap(_.resultPlayerOnThird))
            .filter(_.isDefined)
            .map(p => db.getPlayerById(p.getOrElse(0)).get)
          Ok(views.html.umpires.newaction(gameId, nextBatter.player.player, potentialStealers, pitcher.player.player, newGameActionForm, "", batterPing))

        }
      } getOrElse {
        BadRequest("Please submit the lineups before starting the game.")
      }
    } getOrElse {
      BadRequest("You are not permitted to view this.")
    }
  }

  def submitNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      game.fieldingLineup.map(db.getLineupWithPlayers).flatMap { fieldingLineup =>
        game.battingLineup.map(db.getLineupWithPlayers).map { battingLineup =>

          // Get pitcher and batter to make sure they're correct
          val nextBatter = battingLineup.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty).get
          val pitcher = fieldingLineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get

          val potentialStealers = Seq(game.recentPlay.flatMap(_.resultPlayerOnFirst), game.recentPlay.flatMap(_.resultPlayerOnSecond), game.recentPlay.flatMap(_.resultPlayerOnThird))
            .filter(_.isDefined)
            .map(p => db.getPlayerById(p.getOrElse(0)).get)

          val batterPing = generateBatterPing(game, nextBatter, db.getGameLog(game.game))

          newGameActionForm.bindFromRequest.fold(
            formWithErrors => BadRequest(views.html.umpires.newaction(gameId, nextBatter.player.player, potentialStealers, pitcher.player.player, formWithErrors, "", batterPing)),
            formData => {
              // Make sure batter and pitcher are correct (if submitted wrong, the form may have been submitted by another umpire)
              if (formData.playType != PLAY_TYPE_STEAL && formData.playType != PLAY_TYPE_MULTI_STEAL && formData.batter != nextBatter.player.player.id)
                BadRequest(views.html.umpires.newaction(gameId, nextBatter.player.player, potentialStealers, pitcher.player.player, newGameActionForm.withGlobalError("This is not the current batter. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing))
              else if (formData.pitcher != pitcher.player.player.id)
                BadRequest(views.html.umpires.newaction(gameId, nextBatter.player.player, potentialStealers, pitcher.player.player, newGameActionForm.withGlobalError("This is not the current pitcher. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing))
              else {
                val recentPlay = game.recentPlay.getOrElse(db.initGame(game.game.id))
                val newState = formData.playType match {
                  case PLAY_TYPE_SWING | model.PLAY_TYPE_INFIELD_IN => calc.handleSwing(recentPlay, db.getPlayerWithTypes(formData.pitcher), formData.pitch.get, db.getPlayerWithTypes(formData.batter), formData.swing.get, db.getParkForTeam(game.homeTeam), formData.playType == model.PLAY_TYPE_INFIELD_IN)
                  case PLAY_TYPE_AUTO_K => calc.handleAutoK(recentPlay, db.getPlayerWithTypes(formData.pitcher), db.getPlayerWithTypes(formData.batter))
                  case PLAY_TYPE_AUTO_BB => calc.handleAutoBB(recentPlay, db.getPlayerWithTypes(formData.pitcher), db.getPlayerWithTypes(formData.batter))
                  case PLAY_TYPE_BUNT => calc.handleBunt(recentPlay, pitcher.player.player, formData.pitch.get, nextBatter.player.player, formData.swing.get)
                  case PLAY_TYPE_STEAL => calc.handleSteal(recentPlay, pitcher.player.player, formData.pitch.get, db.getPlayerById(formData.batter).get, formData.swing.get)
                  case PLAY_TYPE_MULTI_STEAL => calc.handleMultiSteal(recentPlay, pitcher.player.player, formData.pitch.get, db.getPlayerById(formData.batter).get, formData.swing.get)
                  case PLAY_TYPE_IBB => calc.handleIBB(recentPlay, db.getPlayerWithTypes(formData.pitcher), db.getPlayerWithTypes(formData.batter))
                }

                if (formData.save) {
                  // Save the updated play in the database
                  db.addGameAction(newState)

                  // Check if we are ending the game or going to extras
                  var shouldEndGame = false
                  val inningFlipped = newState.resultInning != recentPlay.resultInning
                  if (newState.resultInning > 11) {
                    if (newState.resultInning % 2 == 0) { // Going into bottom
                      shouldEndGame = newState.resultScoreHome > newState.resultScoreAway
                    } else { // Going into top
                      shouldEndGame = inningFlipped && newState.resultScoreHome != newState.resultScoreAway
                    }

                    if (inningFlipped && !shouldEndGame && newState.resultInning > 12) { // Going into extras

                      // Determine runners
                      val awayBatting = newState.resultInning % 2 == 1
                      val nextBattingLineup = fieldingLineup // Have to flip for the inning switch not being reflected yet
                      var bPosBack1 = ((if (awayBatting) newState.resultAwayBattingPosition else newState.resultHomeBattingPosition) + 7) % 9 + 1
                      var bPosBack2 = ((if (awayBatting) newState.resultAwayBattingPosition else newState.resultHomeBattingPosition) + 6) % 9 + 1
                      var bPosBack3 = ((if (awayBatting) newState.resultAwayBattingPosition else newState.resultHomeBattingPosition) + 5) % 9 + 1
                      if (bPosBack1 < 1)
                        bPosBack1 = 9 - bPosBack1
                      if (bPosBack2 < 1)
                        bPosBack2 = 9 - bPosBack2
                      if (bPosBack3 < 1)
                        bPosBack3 = 9 - bPosBack3
                      val (onFirst, onSecond, onThird) = if (newState.resultInning > 16) { // Going into 9th, add three runners
                        (
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack3 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack2 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                        )
                      } else if (newState.resultInning > 14) { // Going into 8th, add two runners
                        (
                          None,
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack2 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                        )
                      } else { // Going into 7th, add one runner
                        (
                          None,
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          None
                        )
                      }

                      // Add new runner-placing dummy action
                      val extrasState = new GameAction(0, None, gameId, 0, None, None, None, None, None, None, newState.resultAwayBattingPosition, newState.resultHomeBattingPosition, onFirst, onSecond, onThird, 0, newState.resultScoreAway, newState.resultScoreHome, newState.resultInning, 0, 0)
                      db.addGameAction(extrasState)
                    }
                  }

                  if (shouldEndGame)
                    db.markGameComplete(gameId)

                  Redirect(routes.GameController.viewGame(gameId))
                } else {
                  // Generate the preview and return to the form
                  var preview = formData.playType match {
                    case PLAY_TYPE_AUTO_K => "Auto-K"
                    case PLAY_TYPE_AUTO_BB => "Auto-BB"
                    case PLAY_TYPE_STEAL | PLAY_TYPE_MULTI_STEAL => "Steal"
                    case PLAY_TYPE_BUNT => "Bunt"
                    case PLAY_TYPE_IBB => "IBB"
                    case _ => "Swing"
                  }

                  preview += s": ${newState.swing.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Pitch: ${newState.pitch.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Diff: ${newState.diff.map(_.toString).getOrElse("x")} -> "
                  if (newState.result.contains("FO") && newState.runsScored == 1) {
                    preview += "Sac"
                  } else if (newState.outsTracked == 2) {
                    preview += s"${newState.result.getOrElse("Unknown")} (DP)"
                  } else if (newState.outsTracked == 3) {
                    preview += s"${newState.result.getOrElse("Unknown")} (TP)"
                  } else {
                    preview += newState.result.getOrElse("Unknown")
                  }

                  Ok(views.html.umpires.newaction(gameId, nextBatter.player.player, potentialStealers, pitcher.player.player, newGameActionForm.fill(formData), preview, batterPing))
                }
              }
            }
          )
        }
      } getOrElse {
        BadRequest("Please submit the lineups before starting the game.")
      }
    } getOrElse {
      BadRequest("You are not permitted to view this.")
    }
  }

  def showLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGameWithInfoById(gameId).flatMap { game =>
      (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
        if (db.getGamesForUmpire(ru).exists(g => !g.game.completed && g.game.id == gameId)) {
          var form = lineupForm(team)
          (if (game.awayTeam.id == team.id) game.game.awayLineup.map(db.getLineupWithPlayers) else if (game.homeTeam.id == team.id) game.game.homeLineup.map(db.getLineupWithPlayers) else None).foreach { lineup =>
            form = form.bind(
              lineup
                .filter(e => e.lineupEntry.replacedBy.isEmpty)
                .flatMap { entry =>
                  if (entry.lineupEntry.battingPos == 0)
                    Map("pitcher" -> entry.lineupEntry.player.toString)
                  else
                    Map(
                      s"player${entry.lineupEntry.battingPos}" -> entry.lineupEntry.player.toString,
                      s"p${entry.lineupEntry.battingPos}Pos" -> entry.lineupEntry.position
                    )
                }.toMap
            )
          }
          Ok(views.html.umpires.editlineup(game.game, team, db.getPlayersOnTeam(team), form))
        }
        else
          BadRequest("You are not permitted to view this.")
      }
    } getOrElse {
      BadRequest("You are not permitted to view this.")
    }
  }

  def submitLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGameWithInfoById(gameId).flatMap { game =>
      (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
        if (db.getGamesForUmpire(ru).exists(g => !g.game.completed && g.game.id == gameId))
          lineupForm(team).bindFromRequest().fold(
            formWithErrors => BadRequest(views.html.umpires.editlineup(game.game, team, db.getPlayersOnTeam(team), formWithErrors)),
            formData => {
              val currentLineup = if (game.awayTeam.id == team.id) game.game.awayLineup.map(db.getLineupWithPlayers) else game.game.homeLineup.map(db.getLineupWithPlayers)
              if (currentLineup.isDefined)
                currentLineup.foreach { lineup =>
                  // Update lineup
                  val tupled = formData.tupled
                  val oldPitcher = lineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get
                  if (oldPitcher.player.player.id != formData.pitcher)
                    db.addLineupEntry(oldPitcher.lineupEntry.lineup, formData.pitcher, "P", 0, Some(oldPitcher.lineupEntry.id))
                  for (i <- tupled.indices) {
                    val oldEntry = lineup.find(e => e.lineupEntry.battingPos == (i + 1) && e.lineupEntry.replacedBy.isEmpty).get
                    if (oldEntry.player.player.id != tupled(i)._1 || oldEntry.lineupEntry.position != tupled(i)._2)
                      db.addLineupEntry(oldEntry.lineupEntry.lineup, tupled(i)._1, tupled(i)._2, i + 1, Some(oldEntry.lineupEntry.id))
                  }
                }
              else {
                // New lineup
                val newLineup = db.createLineup
                val tupled = formData.tupled
                db.addLineupEntry(newLineup.id, formData.pitcher, "P", 0)
                for (i <- tupled.indices) {
                  db.addLineupEntry(newLineup.id, tupled(i)._1, tupled(i)._2, i + 1)
                }
                db.setLineup(game.game, newLineup, game.awayTeam.id == team.id)
              }
              Redirect(routes.GameController.viewGame(gameId))
            }
          )
        else
          BadRequest("You are not permitted to view this.")
      }
    } getOrElse {
      BadRequest("You are not permitted to view this.")
    }
  }

  def showDeleteRecentPlayForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).filter(_.recentPlay.isDefined).flatMap { game =>
      db.getGameActionWithPlayers(game.recentPlay.get.id).map { recentPlay =>
        Ok(views.html.umpires.deleteaction(recentPlay, deleteActionForm))
      }
    } getOrElse {
      Unauthorized("You are not permitted to view this.")
    }
  }

  def deleteRecentPlay(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).flatMap { game =>
      db.getGameActionWithPlayers(game.recentPlay.get.id).map { recentPlay =>
        deleteActionForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.deleteaction(recentPlay, formWithErrors)),
          formData =>
            if (formData.id != recentPlay.gameAction.id)
              BadRequest(views.html.umpires.deleteaction(recentPlay, deleteActionForm))
            else {
              db.deleteGameAction(formData.id)
              Redirect(routes.GameController.viewGame(gameId))
            }
        )
      }
    } getOrElse {
      Unauthorized("You do not have permission to view this.")
    }
  }

  def generateBoxScore(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGameWithInfoById(gameId).map { game =>
      // Get everything needed
      val allPlays = db.getGameLog(game.game).map(_.gameAction)
      val awayLineup = game.game.awayLineup.map(id => db.getLineupWithPlayers(id))
      val homeLineup = game.game.homeLineup.map(id => db.getLineupWithPlayers(id))
      val awayScore = game.recentPlay.map(_.resultScoreAway).getOrElse(0)
      val homeScore = game.recentPlay.map(_.resultScoreHome).getOrElse(0)

      // Generate the harder parts
      val line = RedditFormattingService.generateLineScore(allPlays, game.awayTeam.tag, game.homeTeam.tag, game.game.completed)
      val box = RedditFormattingService.generateLineupBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name)
      val pitchers = RedditFormattingService.generatePitcherBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name)

      // Put it all together
      val boxScore = s"##${game.awayTeam.tag} $awayScore - $homeScore ${game.homeTeam.tag}\n\n##LINE\n$line\n\n##BOX\n$box\n\n##PITCHERS\n$pitchers"
      Ok(views.html.umpires.boxscore(game.name, boxScore))
    } getOrElse {
      NotFound("This game does not exist.")
    }
  }

}
