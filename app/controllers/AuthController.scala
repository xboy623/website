package controllers

import java.security.SecureRandom

import javax.inject._
import model.FBDatabase
import play.api.mvc._
import services.RedditService

@Singleton
class AuthController @Inject()(redditService: RedditService, db: FBDatabase, cc: ControllerComponents) extends AbstractController(cc) {

  private var states = Set[String]()

  def redirectToSignIn: Action[AnyContent] = Action { implicit req =>
    val random = SecureRandom.getInstanceStrong.nextInt()
    val newState = Integer.toString(random, 36)

    states += newState
    Found(redditService.oauthConsentUri(newState)).withSession("auth_state" -> newState)
  }

  def signIn: Action[AnyContent] = Action { implicit req =>
    req.getQueryString("error").map { error =>
      Ok(s"Got an error from Reddit: $error\n\nPlease try again later.")
    } getOrElse {
      req.getQueryString("state").flatMap { state =>
        if (states.contains(state)) {
          states -= state
          req.getQueryString("code").map { code =>
            val redditAccount = redditService.getIdentity(code)
            db.getUserByRedditName(redditAccount.name).map { user =>
              Redirect(routes.HomeController.index())
                .withNewSession
                .withSession("uid" -> user.id.toString)
            } getOrElse {
              Redirect(routes.HomeController.index())
                  .withNewSession
                  .withSession("uid" -> db.createUserAccount(redditAccount.name).toString)
            }
          }
        } else {
          Some(BadRequest("Bad request."))
        }
      } getOrElse {
        BadRequest("Bad request.")
      }
    }
  }

  def signOut: Action[AnyContent] = Action {
    Redirect(routes.HomeController.index())
      .withNewSession
  }

}
