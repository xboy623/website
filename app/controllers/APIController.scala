package controllers

import javax.inject.{Inject, Singleton}
import model.{FBDatabase, GameWithRecentPlay, Team}
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class APIController @Inject()(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  private implicit val TeamWrites: Writes[Team] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "tag").write[String] and
      (JsPath \ "park").write[Int]
  )((t: Team) => (t.id, t.name, t.tag, t.park))
  private implicit val GameWrites: Writes[GameWithRecentPlay] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "homeTeam").write[Team] and
      (JsPath \ "awayTeam").write[Team] and
      (JsPath \ "awayScore").write[Int] and
      (JsPath \ "homeScore").write[Int] and
      (JsPath \ "completed").write[Boolean] and
      (JsPath \ "outs").write[Int] and
      (JsPath \ "inning").write[String] and
      (JsPath \ "firstOccupied").write[Boolean] and
      (JsPath \ "secondOccupied").write[Boolean] and
      (JsPath \ "thirdOccupied").write[Boolean] and
      (JsPath \ "id36").write[Option[String]]
    )((g: GameWithRecentPlay) => (g.game.id, g.homeTeam, g.awayTeam, g.recentPlay.map(_.resultScoreAway).getOrElse(0), g.recentPlay.map(_.resultScoreHome).getOrElse(0), g.game.completed, g.recentPlay.map(_.resultOuts).getOrElse(0), g.inning, g.recentPlay.flatMap(_.resultPlayerOnFirst).isDefined, g.recentPlay.flatMap(_.resultPlayerOnSecond).isDefined, g.recentPlay.flatMap(_.resultPlayerOnThird).isDefined, g.game.id36))

  def gamesInSession(season: Int, session: Int): Action[AnyContent] = Action {
    Ok(Json.toJson(db.getGamesInSession(season, session)))
  }

}
