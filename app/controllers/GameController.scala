package controllers

import akka.util.ByteString
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.http.HttpEntity
import play.api.mvc._
import services.{GameStateImageGeneratorService, RedditFormattingService}

@Singleton
class GameController @Inject()(gameStateImageGenerator: GameStateImageGeneratorService)(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.games.index(db.getActiveGames, db.getUpcomingGames)(req, user))
  }

  def viewGame(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getGameWithInfoById(gameId).map { game =>
      val editable = user.exists(u => db.getGamesForUmpire(u).exists(g => !g.game.completed && g.game.id == gameId))
      Ok(views.html.games.view(game, if (game.game.completed) db.getGameLog(game.game).reverse else db.getGameLog(game.game), game.game.awayLineup.map(db.getLineupWithPlayers), game.game.homeLineup.map(db.getLineupWithPlayers), editable)(req, user))
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGameImage(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getGameWithInfoById(gameId).map { game =>
      Result(
        header = ResponseHeader(200),
        body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateImage(game, game.battingLineup.map(db.getLineupWithPlayers), game.fieldingLineup.map(db.getLineupWithPlayers))), Some("image/svg+xml"))
      )
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGamePreviewImage(gameId: Int): Action[AnyContent] = messagesActionBuilder {
    db.getGameWithInfoById(gameId).map { game =>
      Result(
        header = ResponseHeader(200),
        body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateBasicImage(game)), Some("image/png"))
      )
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

}
