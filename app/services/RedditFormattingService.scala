package services

import model._

object RedditFormattingService {

  def generateLineScore(allPlays: Seq[GameAction], awayTeam: String, homeTeam: String, complete: Boolean): String = {

    // Always present
    var line1 = "||"
    var line2 = "|:--|"
    var line3 = s"|**$awayTeam**|"
    var line4 = s"|**$homeTeam**|"

    // Trackers
    var currentInning = 1
    var runCount = 0

    // Track max inning played
    val maxInning = if (allPlays.isEmpty) 0 else allPlays.maxBy(_.resultInning).resultInning

    // Loop all innings played, but at least 6
    for (i <- 1 to Math.max(maxInning, 12)) {
      // Determine if the current inning being added is the active inning
      val italics = i == maxInning && !complete
      // Add away score if odd, home score if even
      if (i % 2 == 1) {
        line1 += s"${(i + 1) / 2}|"
        line2 += ":--|"

        // Only add score content if we have reached that inning
        if (i <= maxInning)
          line3 += s"${if (italics) "*" else ""}${allPlays.filter(_.resultInning == i).map(_.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line3 += "|"
      } else {
        if (i <= maxInning)
          line4 += s"${if (italics) "*" else ""}${allPlays.filter(_.resultInning == i).map(_.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line4 += "|"
      }
    }

    // Return lines combined
    s"\n$line1\n$line2\n$line3\n$line4"
  }

  def generateLineupBox(allPlays: Seq[GameAction], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String): String = {
    val header = s"|\\#|$awayTeam|Pos|PA|H|RBI|K|BA|\\#|$homeTeam|Pos|PA|H|RBI|K|BA|\n" +
      "|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var lineupBox = ""
    for (i <- 1 to 9) {
      // Figure out how many rows we need for this batting position
      val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

      // Now start putting in the players, or blank lines if none
      for (j <- 0 until rows) {
        val awayPlayer = aways.flatMap(_.lift(j))
        lineupBox += awayPlayer.map(entry => {
          s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.name}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.playType != PLAY_TYPE_STEAL && p.playType != PLAY_TYPE_MULTI_STEAL))}|" +
            s"${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.result.contains("HR") || p.result.contains("3B") || p.result.contains("2B") || p.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.contains(entry.player.player.id)).foldLeft(0)(_ + _.runsScored)}|" +
            s"${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.result.contains("K") || p.result.contains("Auto-K") || p.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.ba)}|"
        }).getOrElse("|||||||||")
        val homePlayer = homes.flatMap(_.lift(j))
        lineupBox += homePlayer.map(entry => {
          s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.name}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.playType != PLAY_TYPE_STEAL && p.playType != PLAY_TYPE_MULTI_STEAL))}|" +
            s"${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.result.contains("HR") || p.result.contains("3B") || p.result.contains("2B") || p.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.contains(entry.player.player.id)).foldLeft(0)(_ + _.runsScored)}|" +
            s"${allPlays.count(p => p.batter.contains(entry.player.player.id) && (p.result.contains("K") || p.result.contains("Auto-K") || p.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.ba)}|\n"
        }).getOrElse("|||||||||\n")
      }
    }

    // Return the combined string
    s"$header\n$lineupBox"
  }

  def generatePitcherBox(allPlays: Seq[GameAction], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String): String = {
    val header = s"|$awayTeam|IP|ER|H|BB|K|ERA|$homeTeam|IP|ER|H|BB|K|ERA|\n" +
      s"|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var pitcherBox = ""

    // Figure out how many rows we need for this table
    val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

    // Now start putting in the players, or blank lines if none
    for (j <- 0 until rows) {
      val awayPlayer = aways.flatMap(_.lift(j))
      pitcherBox += awayPlayer.map(entry => {
        s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.name}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.contains(entry.player.player.id)).foldLeft(0)(_ + _.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.filter(_.pitcher.contains(entry.player.player.id)).foldLeft(0)(_ + _.runsScored)}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("HR") || p.result.contains("3B") || p.result.contains("2B") || p.result.contains("1B")))}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("BB") || p.result.contains("Auto-BB")))}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("K") || p.result.contains("Auto-K") || p.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.era)}|"
      }).getOrElse("||||||||")
      val homePlayer = homes.flatMap(_.lift(j))
      pitcherBox += homePlayer.map(entry => {
        s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.name}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.contains(entry.player.player.id)).foldLeft(0)(_ + _.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.filter(_.pitcher.contains(entry.player.player.id)).foldLeft(0)(_ + _.runsScored)}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("HR") || p.result.contains("3B") || p.result.contains("2B") || p.result.contains("1B")))}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("BB") || p.result.contains("Auto-BB")))}|" +
          s"${allPlays.count(p => p.pitcher.contains(entry.player.player.id) && (p.result.contains("K") || p.result.contains("Auto-K") || p.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.era)}|\n"
      }).getOrElse("||||||||")
    }

    // Combine and return
    s"$header\n$pitcherBox"
  }

}
