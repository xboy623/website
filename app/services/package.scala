package object services {
  case class RedditUserInfo(id: String, name: String, isEmailVerified: Boolean, isSuspended: Boolean, linkKarma: Int, commentKarma: Int, creationTimestamp: Long)
}
