package services

import javax.inject.{Inject, Singleton}
import model._

// There's a lot of stuff in this file that could *probably* be refactored and made more pretty,
// although I probably won't revisit it since it's reasonably straightforward for now, albeit verbose
@Singleton
class MLRCalculatorService @Inject()(implicit db: FBDatabase) {

  private val INFIELD_IN_RANGE: CalcRange = CalcRange(0, 0, 0, 6, 0, 0, 0, 0, -3, -3)

  def calculateDiff(swing: Int, pitch: Int): Int = {
    val max = Math.max(swing, pitch)
    val min = Math.min(swing, pitch)
    Math.min(max - min, 1000 - max + min)
  }

  def calculateRanges(batter: PlayerWithTypes, pitcher: PlayerWithTypes, park: Park, infieldIn: Boolean = false): CalcRange = {
    // Add linear ranges
    val pitchingType = pitcher.pitchingType.getOrElse(db.getPitchingTypeByShortcode("POS").get)
    var combinedRanges = batter.battingType + pitchingType
    if (pitcher.pitchingBonus.isDefined && batter.player.righthanded == pitcher.player.righthanded)
      combinedRanges += pitcher.pitchingBonus.get

    // Now do the whole park calculation bit
    val parkChanges = CalcRange(
      Math.round(park.factorHr * combinedRanges.rangeHR).toInt - combinedRanges.rangeHR,
      Math.round(park.factor3b * combinedRanges.range3B).toInt - combinedRanges.range3B,
      Math.round(park.factor2b * combinedRanges.range2B).toInt - combinedRanges.range2B,
      Math.round(park.factor1b * combinedRanges.range1B).toInt - combinedRanges.range1B,
      Math.round(park.factorBb * combinedRanges.rangeBB).toInt - combinedRanges.rangeBB,
      0, 0, 0, 0, 0
    )
    var parkChangesSum = parkChanges.rangeHR + parkChanges.range3B + parkChanges.range2B + parkChanges.range1B + parkChanges.rangeBB
    val direction = if (parkChangesSum < 0) 1 else -1

    var index = -1
    while (parkChangesSum != 0) {
      index += 1
      index %= 5
      val currentRange = index match {
        case 0 => combinedRanges.rangeFO + parkChanges.rangeFO
        case 1 => combinedRanges.rangeK + parkChanges.rangeFO
        case 2 => combinedRanges.rangePO + parkChanges.rangeFO
        case 3 => combinedRanges.rangeRGO + parkChanges.rangeFO
        case 4 => combinedRanges.rangeLGO + parkChanges.rangeFO
      }
      if (currentRange > 0)
        index match {
          case 0 => parkChanges.rangeFO += direction
          case 1 => parkChanges.rangeK += direction
          case 2 => parkChanges.rangePO += direction
          case 3 => parkChanges.rangeRGO += direction
          case 4 => parkChanges.rangeLGO += direction
        }
      parkChangesSum += direction
    }

    if (infieldIn)
      combinedRanges + parkChanges + INFIELD_IN_RANGE
    else
      combinedRanges + parkChanges
  }

  def calculateSwing(batter: PlayerWithTypes, swing: Int, pitcher: PlayerWithTypes, pitch: Int, park: Park, infieldIn: Boolean = false): SwingResult = {
    val diff = calculateDiff(swing, pitch)
    val ranges = calculateRanges(batter, pitcher, park, infieldIn).absolute

    if (diff < ranges.rangeHR)
      SwingResult(diff, "HR")
    else if (diff < ranges.range3B)
      SwingResult(diff, "3B")
    else if (diff < ranges.range2B)
      SwingResult(diff, "2B")
    else if (diff < ranges.range1B)
      SwingResult(diff, "1B")
    else if (diff < ranges.rangeBB)
      SwingResult(diff, "BB")
    else if (diff < ranges.rangeFO)
      SwingResult(diff, "FO")
    else if (diff < ranges.rangeK)
      SwingResult(diff, "K")
    else if (diff < ranges.rangePO)
      SwingResult(diff, "PO")
    else if (diff < ranges.rangeRGO)
      SwingResult(diff, "RGO")
    else
      SwingResult(diff, "LGO")
  }

  def handleSwing(currentState: GameAction, pitcher: PlayerWithTypes, pitch: Int, batter: PlayerWithTypes, swing: Int, park: Park, infieldIn: Boolean): GameAction = {
    // Determine swing result
    val result = calculateSwing(batter, swing, pitcher, pitch, park, infieldIn)

    // Calculate the new game state changes through this monstrosity (TODO NEEDS REFACTORING BAD)
    val isAwayBatting = currentState.resultInning % 2 == 1
    var (scorers, newOuts, onFirst, onSecond, onThird): (Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = result.result match {
      case "HR" =>
        (Seq(currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird, Some(batter.player.id)), 0, None, None, None)
      case "3B" =>
        (Seq(currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird), 0, None, None, Some(batter.player.id))
      case "2B" =>
        if (currentState.resultOuts == 2)
          (Seq(currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird), 0, None, Some(batter.player.id), None)
        else
          (Seq(currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird), 0, None, Some(batter.player.id), currentState.resultPlayerOnFirst)
      case "1B" =>
        if (currentState.resultOuts == 2)
          (Seq(currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird), 0, Some(batter.player.id), None, currentState.resultPlayerOnFirst)
        else
          (Seq(currentState.resultPlayerOnThird), 0, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
      case "BB" =>
        if (currentState.resultPlayerOnFirst.isDefined) {
          if (currentState.resultPlayerOnSecond.isDefined)
            (Seq(currentState.resultPlayerOnThird), 0, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
          else
            (Seq(), 0, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnThird)
        } else
          (Seq(), 0, Some(batter.player.id), currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
      case "FO" =>
        (Seq(currentState.resultPlayerOnThird), 1, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, None)
      case "K" | "PO" =>
        (Seq(), 1, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
      case "RGO" =>
        if (infieldIn) {
          if (currentState.resultPlayerOnFirst.isDefined && currentState.resultPlayerOnSecond.isDefined && currentState.resultPlayerOnThird.isDefined)
            (Seq(), 1, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
          else
            (Seq(), 1, None, currentState.resultPlayerOnSecond.flatMap(_ => currentState.resultPlayerOnFirst), currentState.resultPlayerOnThird)
        }
          else
            (Seq(currentState.resultPlayerOnThird), if (currentState.resultPlayerOnFirst.isDefined) 2 else 1, None, None, currentState.resultPlayerOnSecond)
      case "LGO" =>
        if (infieldIn) {
          if (currentState.resultPlayerOnFirst.isDefined && currentState.resultPlayerOnSecond.isDefined && currentState.resultPlayerOnThird.isDefined)
            (Seq(), 1, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
          else
            (Seq(), 1, None, currentState.resultPlayerOnSecond.flatMap(_ => currentState.resultPlayerOnFirst), currentState.resultPlayerOnThird)
        } else {
          if (currentState.resultPlayerOnFirst.isDefined) {
            if (currentState.resultPlayerOnSecond.isDefined) {
              (Seq(currentState.resultPlayerOnThird), if (result.diff > 495) 3 else 2, None, None, currentState.resultPlayerOnSecond)
            } else {
              (Seq(currentState.resultPlayerOnThird), 2, None, None, None)
            }
          } else {
            (Seq(currentState.resultPlayerOnThird), 1, None, currentState.resultPlayerOnSecond, None)
          }
        }
    }

    // Update the outs counter and rollover inning if needed
    val resOuts = if (currentState.resultOuts + newOuts >= 3) 0 else currentState.resultOuts + newOuts
    val resInning = if (currentState.resultOuts + newOuts >= 3) currentState.resultInning + 1 else currentState.resultInning

    var resAwayScore = currentState.resultScoreAway
    var resHomeScore = currentState.resultScoreHome

    // Updates scores if we didn't hit 3 out there
    if (resInning == currentState.resultInning) {
      if (isAwayBatting)
        resAwayScore += scorers.count(_.isDefined)
      else
        resHomeScore += scorers.count(_.isDefined)
    } else {
      // If inning flipped, remove baserunners
      onFirst = None
      onSecond = None
      onThird = None
    }

    // Increment the correct lineup batter position
    val resAwayBPos = if (isAwayBatting) (currentState.resultAwayBattingPosition % 9) + 1 else currentState.resultAwayBattingPosition
    val resHomeBPos = if (!isAwayBatting) (currentState.resultHomeBattingPosition % 9) + 1 else currentState.resultHomeBattingPosition

    // Return the new game state
    new GameAction(0, None, currentState.gameId, if (infieldIn) PLAY_TYPE_INFIELD_IN else PLAY_TYPE_SWING, Some(pitcher.player.id), Some(pitch), Some(batter.player.id), Some(swing), Some(result.diff), Some(result.result), resAwayBPos, resHomeBPos, onFirst, onSecond, onThird, resOuts, resAwayScore, resHomeScore, resInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, Math.min(currentState.resultOuts + newOuts, 3) - currentState.resultOuts)
  }

  def handleAutoK(currentState: GameAction, pitcher: PlayerWithTypes, batter: PlayerWithTypes): GameAction = {
    // Increase outs, and rollover inning counter if needed
    val resOuts = if (currentState.resultOuts + 1 == 3) 0 else currentState.resultOuts + 1
    val resInning = if (resOuts == 0) currentState.resultInning + 1 else currentState.resultInning

    // Remove batters if the inning is flipping
    val onFirst = if (resInning == currentState.resultInning) currentState.resultPlayerOnFirst else None
    val onSecond = if (resInning == currentState.resultInning) currentState.resultPlayerOnSecond else None
    val onThird = if (resInning == currentState.resultInning) currentState.resultPlayerOnThird else None

    // Increment the correct lineup batter position
    val isAwayBatting = currentState.resultInning % 2 == 1
    val resAwayBPos = if (isAwayBatting) (currentState.resultAwayBattingPosition % 9) + 1 else currentState.resultAwayBattingPosition
    val resHomeBPos = if (!isAwayBatting) (currentState.resultHomeBattingPosition % 9) + 1 else currentState.resultHomeBattingPosition

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_AUTO_K, Some(pitcher.player.id), None, Some(batter.player.id), None, None, Some("Auto K"), resAwayBPos, resHomeBPos, onFirst, onSecond, onThird, resOuts, currentState.resultScoreAway, currentState.resultScoreHome, resInning, 0, 1)
  }

  def handleAutoBB(currentState: GameAction, pitcher: PlayerWithTypes, batter: PlayerWithTypes): GameAction = {
    // Determine if any runners score/advance (same as normal walk)
    val (scoring, onFirst, onSecond, onThird) =
      if (currentState.resultPlayerOnFirst.isDefined) {
        if (currentState.resultPlayerOnSecond.isDefined) {
          (currentState.resultPlayerOnThird.isDefined, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
        } else {
          (false, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnThird)
        }
      } else {
        (false, Some(batter.player.id), currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
      }

    // Update score if needed
    val isAwayBatting = currentState.resultInning % 2 == 1
    val resAwayScore = if (isAwayBatting && scoring) currentState.resultScoreAway + 1 else currentState.resultScoreAway
    val resHomeScore = if (!isAwayBatting && scoring) currentState.resultScoreHome + 1 else currentState.resultScoreHome

    // Increment the correct lineup batter position
    val resAwayBPos = if (isAwayBatting) (currentState.resultAwayBattingPosition % 9) + 1 else currentState.resultAwayBattingPosition
    val resHomeBPos = if (!isAwayBatting) (currentState.resultHomeBattingPosition % 9) + 1 else currentState.resultHomeBattingPosition

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_AUTO_BB, Some(pitcher.player.id), None, Some(batter.player.id), None, None, Some("Auto BB"), resAwayBPos, resHomeBPos, onFirst, onSecond, onThird, currentState.resultOuts, resAwayScore, resHomeScore, currentState.resultInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, 0)
  }

  def handleIBB(currentState: GameAction, pitcher: PlayerWithTypes, batter: PlayerWithTypes): GameAction = {
    // Determine if any runners score/advance (same as normal walk)
    val (scoring, onFirst, onSecond, onThird) =
      if (currentState.resultPlayerOnFirst.isDefined) {
        if (currentState.resultPlayerOnSecond.isDefined) {
          (currentState.resultPlayerOnThird.isDefined, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
        } else {
          (false, Some(batter.player.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnThird)
        }
      } else {
        (false, Some(batter.player.id), currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
      }

    // Update score if needed
    val isAwayBatting = currentState.resultInning % 2 == 1
    val resAwayScore = if (isAwayBatting && scoring) currentState.resultScoreAway + 1 else currentState.resultScoreAway
    val resHomeScore = if (!isAwayBatting && scoring) currentState.resultScoreHome + 1 else currentState.resultScoreHome

    // Increment the correct lineup batter position
    val resAwayBPos = if (isAwayBatting) (currentState.resultAwayBattingPosition % 9) + 1 else currentState.resultAwayBattingPosition
    val resHomeBPos = if (!isAwayBatting) (currentState.resultHomeBattingPosition % 9) + 1 else currentState.resultHomeBattingPosition

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_AUTO_BB, Some(pitcher.player.id), None, Some(batter.player.id), None, None, Some("IBB"), resAwayBPos, resHomeBPos, onFirst, onSecond, onThird, currentState.resultOuts, resAwayScore, resHomeScore, currentState.resultInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, 0)
  }

  def handleBunt(currentState: GameAction, pitcher: Player, pitch: Int, batter: Player, swing: Int): GameAction = {
    val diff = calculateDiff(swing, pitch)

    // Determine output
    var (result, scorers, newOuts, onFirst, onSecond, onThird): (String, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) =  {
      if (diff <= 50) {
        ("Bunt 1B", Seq(currentState.resultPlayerOnThird), 0, Some(batter.id), currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
      } else if (diff <= 375) {
        if (currentState.resultPlayerOnThird.isDefined) {
          if (currentState.resultPlayerOnSecond.isDefined) {
            ("Bunt Sac", Seq(), 1, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
          } else {
            ("Bunt Sac", Seq(), 1, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnThird)
          }
        } else {
          ("Bunt Sac", Seq(), 1, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
        }
      } else if (diff <= 475) {
        ("Bunt K", Seq(), 1, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, currentState.resultPlayerOnThird)
      } else {
        (if (currentState.resultPlayerOnFirst.isDefined) "Bunt DP" else "Bunt Sac RS", Seq(currentState.resultPlayerOnThird), if (currentState.resultPlayerOnFirst.isDefined) 2 else 1, None, None, currentState.resultPlayerOnSecond)
      }
    }

    // Update the outs counter and rollover inning if needed
    val resOuts = if (currentState.resultOuts + newOuts >= 3) 0 else currentState.resultOuts + newOuts
    val resInning = if (currentState.resultOuts + newOuts >= 3) currentState.resultInning + 1 else currentState.resultInning

    var resAwayScore = currentState.resultScoreAway
    var resHomeScore = currentState.resultScoreHome

    // Updates scores if we didn't hit 3 out there
    val isAwayBatting = currentState.resultInning % 2 == 1
    if (resInning == currentState.resultInning) {
      if (isAwayBatting)
        resAwayScore += scorers.count(_.isDefined)
      else
        resHomeScore += scorers.count(_.isDefined)
    } else {
      // If inning flipped, remove baserunners
      onFirst = None
      onSecond = None
      onThird = None
    }

    // Increment the correct lineup batter position
    val resAwayBPos = if (isAwayBatting) (currentState.resultAwayBattingPosition % 9) + 1 else currentState.resultAwayBattingPosition
    val resHomeBPos = if (!isAwayBatting) (currentState.resultHomeBattingPosition % 9) + 1 else currentState.resultHomeBattingPosition

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_BUNT, Some(pitcher.id), Some(pitch), Some(batter.id), Some(swing), Some(diff), Some(result), resAwayBPos, resHomeBPos, onFirst, onSecond, onThird, resOuts, resAwayScore, resHomeScore, resInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, Math.min(currentState.resultOuts + newOuts, 3) - currentState.resultOuts)

  }

  def handleSteal(currentState: GameAction, pitcher: Player, pitch: Int, stealer: Player, steal: Int): GameAction = {
    val diff = calculateDiff(steal, pitch)

    var (result, scorers, newOuts, onFirst, onSecond, onThird): (String, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = {
      if (currentState.resultPlayerOnFirst.contains(stealer.id)) { // Stealing second
        if (diff <= 300)
          ("Steal 2B", Seq(), 0, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnThird)
        else
          ("CS 2B", Seq(), 1, None, None, currentState.resultPlayerOnThird)
      } else if (currentState.resultPlayerOnSecond.contains(stealer.id)) { // Stealing third
        if (diff <= 150)
          ("Steal 3B", Seq(), 0, currentState.resultPlayerOnFirst, None, currentState.resultPlayerOnSecond)
        else
          ("CS 3B", Seq(), 1, currentState.resultPlayerOnFirst, None, None)
      } else { // Stealing home
        if (diff <= 25)
          ("Steal Home", Seq(currentState.resultPlayerOnThird), 0, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, None)
        else
          ("CS Home", Seq(), 1, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond, None)
      }
    }

    // Update the outs counter and rollover inning if needed
    val resOuts = if (currentState.resultOuts + newOuts >= 3) 0 else currentState.resultOuts + newOuts
    val resInning = if (currentState.resultOuts + newOuts >= 3) currentState.resultInning + 1 else currentState.resultInning

    var resAwayScore = currentState.resultScoreAway
    var resHomeScore = currentState.resultScoreHome

    // Updates scores if we didn't hit 3 out there
    val isAwayBatting = currentState.resultInning % 2 == 1
    if (resInning == currentState.resultInning) {
      if (isAwayBatting)
        resAwayScore += scorers.count(_.isDefined)
      else
        resHomeScore += scorers.count(_.isDefined)
    } else {
      // If inning flipped, remove baserunners
      onFirst = None
      onSecond = None
      onThird = None
    }

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_STEAL, Some(pitcher.id), Some(pitch), Some(stealer.id), Some(steal), Some(diff), Some(result), currentState.resultAwayBattingPosition, currentState.resultHomeBattingPosition, onFirst, onSecond, onThird, resOuts, resAwayScore, resHomeScore, resInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, Math.min(currentState.resultOuts + newOuts, 3) - currentState.resultOuts)
  }

  def handleMultiSteal(currentState: GameAction, pitcher: Player, pitch: Int, stealer: Player, steal: Int): GameAction = {
    val diff = calculateDiff(steal, pitch)

    var (result, scorers, newOuts, onFirst, onSecond, onThird): (String, Seq[Option[Int]], Int, Option[Int], Option[Int], Option[Int]) = {
      if (currentState.resultPlayerOnSecond.contains(stealer.id)) { // Stealing second and third
        if (diff <= 150)
          ("MSteal 3B", Seq(), 0, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
        else
          ("CMS 3B", Seq(), 1, None, currentState.resultPlayerOnFirst, None)
      } else { // Stealing home
        if (diff <= 25)
          ("MSteal Home", Seq(currentState.resultPlayerOnThird), 0, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
        else
          ("CMS Home", Seq(), 1, None, currentState.resultPlayerOnFirst, currentState.resultPlayerOnSecond)
      }
    }

    // Update the outs counter and rollover inning if needed
    val resOuts = if (currentState.resultOuts + newOuts >= 3) 0 else currentState.resultOuts + newOuts
    val resInning = if (currentState.resultOuts + newOuts >= 3) currentState.resultInning + 1 else currentState.resultInning

    var resAwayScore = currentState.resultScoreAway
    var resHomeScore = currentState.resultScoreHome

    // Updates scores if we didn't hit 3 out there
    val isAwayBatting = currentState.resultInning % 2 == 1
    if (resInning == currentState.resultInning) {
      if (isAwayBatting)
        resAwayScore += scorers.count(_.isDefined)
      else
        resHomeScore += scorers.count(_.isDefined)
    } else {
      // If inning flipped, remove baserunners
      onFirst = None
      onSecond = None
      onThird = None
    }

    // Return the new game state
    new GameAction(0, None, currentState.gameId, PLAY_TYPE_MULTI_STEAL, Some(pitcher.id), Some(pitch), Some(stealer.id), Some(steal), Some(diff), Some(result), currentState.resultAwayBattingPosition, currentState.resultHomeBattingPosition, onFirst, onSecond, onThird, resOuts, resAwayScore, resHomeScore, resInning, if (isAwayBatting) resAwayScore - currentState.resultScoreAway else resHomeScore - currentState.resultScoreHome, Math.min(currentState.resultOuts + newOuts, 3) - currentState.resultOuts)
  }

}
