package services

import java.awt.{BasicStroke, Color, Polygon}
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream

import javax.imageio.ImageIO
import javax.inject.{Inject, Singleton}
import model.{GameWithRecentPlay, LineupEntryWithPlayer}
import play.api.Environment

@Singleton
class GameStateImageGeneratorService @Inject()(env: Environment) {

  def generateImage(game: GameWithRecentPlay, battingLineup: Option[Seq[LineupEntryWithPlayer]], fieldingLineup: Option[Seq[LineupEntryWithPlayer]]): String = {
    // Load the template file
    val baseImgStream = env.resourceAsStream("public/images/diamond_template.svg").get
    val baos = new ByteArrayOutputStream()
    val buf = new Array[Byte](1024)
    var len = 0
    while ( {
      len = baseImgStream.read(buf); len
    } != -1)
      baos.write(buf, 0, len)

    // Copy the data into a string for replacing (SVG is plaintext anyway)
    baseImgStream.close()
    val svgData = new String(baos.toByteArray, "UTF-8")
    baos.close()

    // Helper to pull last name of player (TODO separate first/last name in DB)
    def lastName: LineupEntryWithPlayer => String = _.player.player.name.split(" ").last

    // Get all the fielders' names
    val pitcher = fieldingLineup.flatMap(_.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val catcher = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "C" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val firstBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "1B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val secondBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "2B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val thirdBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "3B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val shortstop = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "SS" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val leftFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "LF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val centerFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "CF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val rightFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "RF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")

    // Get the batter and baserunners
    val nextBatter = battingLineup.flatMap(_.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty))
    val runnerOnFirst = game.recentPlay.flatMap(_.resultPlayerOnFirst).flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")
    val runnerOnSecond = game.recentPlay.flatMap(_.resultPlayerOnSecond).flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")
    val runnerOnThird = game.recentPlay.flatMap(_.resultPlayerOnThird).flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")

    // Replace all the placeholders and return
    svgData
      .replace("$PLAYER_P$", pitcher)
      .replace("$PLAYER_C$", catcher)
      .replace("$PLAYER_1B$", firstBasemen)
      .replace("$PLAYER_2B$", secondBasemen)
      .replace("$PLAYER_3B$", thirdBasemen)
      .replace("$PLAYER_SS$", shortstop)
      .replace("$PLAYER_LF$", leftFielder)
      .replace("$PLAYER_CF$", centerFielder)
      .replace("$PLAYER_RF$", rightFielder)
      .replace("$PLAYER_R1$", runnerOnFirst)
      .replace("$PLAYER_R2$", runnerOnSecond)
      .replace("$PLAYER_R3$", runnerOnThird)
      .replace("$1BFILL$", if (game.recentPlay.flatMap(_.resultPlayerOnFirst).isDefined) "black" else "white")
      .replace("$2BFILL$", if (game.recentPlay.flatMap(_.resultPlayerOnSecond).isDefined) "black" else "white")
      .replace("$3BFILL$", if (game.recentPlay.flatMap(_.resultPlayerOnThird).isDefined) "black" else "white")
      .replace("$PLAYER_BL$", nextBatter.filterNot(_.player.player.righthanded).map(lastName).getOrElse(""))
      .replace("$PLAYER_BR$", nextBatter.filter(_.player.player.righthanded).map(lastName).getOrElse(""))
  }

  private val HOME_PLATE_POLYGON = new Polygon(Array(80, 120, 120, 100, 80), Array(150, 150, 170, 190, 170), 5)
  private val FIRST_BASE_POLYGON = new Polygon(Array(190, 160, 130, 160), Array(100, 70, 100, 130), 4)
  private val SECOND_BASE_POLYGON = new Polygon(Array(100, 70, 100, 130), Array(10, 40, 70, 40), 4)
  private val THIRD_BASE_POLYGON = new Polygon(Array(10, 40, 70, 40), Array(100, 70, 100, 130), 4)

  def generateBasicImage(game: GameWithRecentPlay): Array[Byte] = {
    // Create empty image
    val img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB)
    val g = img.createGraphics()

    // Background
    g.setColor(Color.white)
    g.setStroke(new BasicStroke(5))
    g.fillRect(0, 0, img.getWidth, img.getHeight)

    // Home plate
    g.setColor(Color.black)
    g.drawPolygon(HOME_PLATE_POLYGON)

    val onFirst = game.recentPlay.flatMap(_.resultPlayerOnFirst).isDefined
    val onSecond = game.recentPlay.flatMap(_.resultPlayerOnSecond).isDefined
    val onThird = game.recentPlay.flatMap(_.resultPlayerOnThird).isDefined

    // First base
    if (onFirst)
      g.fillPolygon(FIRST_BASE_POLYGON)
    else
      g.drawPolygon(FIRST_BASE_POLYGON)

    // Second base
    if (onSecond)
      g.fillPolygon(SECOND_BASE_POLYGON)
    else
      g.drawPolygon(SECOND_BASE_POLYGON)

    if (onThird)
      g.fillPolygon(THIRD_BASE_POLYGON)
    else
      g.drawPolygon(THIRD_BASE_POLYGON)

    // Write as byte[]
    val baos = new ByteArrayOutputStream()
    ImageIO.write(img, "png", baos)
    baos.toByteArray
  }

}
