package services

import java.util

import javax.inject.{Inject, Singleton}
import org.commonmark.ext.autolink.AutolinkExtension
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension
import org.commonmark.ext.gfm.tables.{TableBlock, TablesExtension}
import org.commonmark.ext.heading.anchor.HeadingAnchorExtension
import org.commonmark.ext.ins.InsExtension
import org.commonmark.node.Node
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.{AttributeProvider, HtmlRenderer}
import play.twirl.api.Html

import scala.collection.JavaConverters._

@Singleton
class MarkdownFormatterService @Inject()() extends AttributeProvider {

  private val markdownExtensions = Seq(
    AutolinkExtension.create(),
    StrikethroughExtension.create(),
    TablesExtension.create(),
    HeadingAnchorExtension.create(),
    InsExtension.create()
  ).asJava

  private val markdownParser = Parser.builder()
    .extensions(markdownExtensions)
    .build()

  private val markdownRenderer = HtmlRenderer.builder()
    .extensions(markdownExtensions)
    .attributeProviderFactory(_ => this)
    .build()

  override def setAttributes(node: Node, tagName: String, attributes: util.Map[String, String]): Unit = {
    node match {
      case _: TableBlock => attributes.put("class", "table table-striped")
      case _ =>
    }
  }

  def parseMarkdown(body: String): Html = Html(markdownRenderer.render(markdownParser.parse(body)))

}
