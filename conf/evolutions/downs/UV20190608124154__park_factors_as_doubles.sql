alter table parks
    modify factor_hr float not null,
    modify factor_3b float not null,
    modify factor_2b float not null,
    modify factor_1b float not null,
    modify factor_bb float not null;
