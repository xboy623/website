ALTER TABLE game_actions
    DROP COLUMN result_away_batting_position,
    DROP COLUMN result_home_batting_position;
