ALTER TABLE player_applications
    DROP FOREIGN KEY fk_player_application_rejecter,
    DROP COLUMN responded_at,
    DROP COLUMN responded_by,
    DROP COLUMN reject_message,
    DROP COLUMN status,
    DROP COLUMN user;
