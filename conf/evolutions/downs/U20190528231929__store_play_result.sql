ALTER TABLE game_actions
    DROP COLUMN diff,
    DROP COLUMN result;
