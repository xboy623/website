ALTER TABLE game_actions
    DROP COLUMN runs_scored,
    DROP COLUMN outs_tracked;
