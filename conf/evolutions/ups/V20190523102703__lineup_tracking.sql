CREATE TABLE IF NOT EXISTS lineups
(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS lineup_entries
(
    id          INT        NOT NULL AUTO_INCREMENT PRIMARY KEY,
    lineup      INT        NOT NULL,
    player      INT        NOT NULL,
    position    VARCHAR(2) NOT NULL,
    batting_pos INT        NOT NULL,
    replaced_by INT        NULL,
    FOREIGN KEY lineup_substitutions_lineup (lineup) REFERENCES lineups (id),
    FOREIGN KEY lineup_substitutions_player (player) REFERENCES players (id),
    FOREIGN KEY lineup_substitutions_replaced_by (replaced_by) REFERENCES lineup_entries (id)
);

ALTER TABLE games
    ADD COLUMN away_lineup INT NULL,
    ADD COLUMN home_lineup INT NULL,
    ADD FOREIGN KEY game_away_lineup (away_lineup) REFERENCES lineups (id),
    ADD FOREIGN KEY game_home_lineup (home_lineup) REFERENCES lineups (id);
