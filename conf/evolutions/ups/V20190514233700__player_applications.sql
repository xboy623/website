-- Stores player applications

CREATE TABLE IF NOT EXISTS player_applications
(
    id                         INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    requested_name             VARCHAR(60)        NOT NULL,
    is_returning_player        BOOL               NOT NULL,
    is_willing_to_join_discord BOOL               NOT NULL,
    position_primary           VARCHAR(2)         NOT NULL,
    position_secondary         VARCHAR(2)         NULL,
    is_right_handed            BOOL               NOT NULL,
    batting_type               INT                NOT NULL,
    pitching_type              INT                NULL,
    pitching_bonus             INT                NULL,
    FOREIGN KEY fk_player_application_batting_type (batting_type) REFERENCES batting_types (id),
    FOREIGN KEY fk_player_application_pitching_type (pitching_type) REFERENCES pitching_types (id),
    FOREIGN KEY fk_player_application_pitching_bonus (pitching_bonus) REFERENCES pitching_bonuses (id)
);
