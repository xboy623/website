ALTER TABLE game_actions
    ADD COLUMN diff INT NULL AFTER swing,
    ADD COLUMN result VARCHAR(10) NULL AFTER diff;
