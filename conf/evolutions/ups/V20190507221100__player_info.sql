-- Adds batting and pitching types with pitching bonuses

CREATE TABLE IF NOT EXISTS batting_types
(
    id        INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    shortcode VARCHAR(3)         NOT NULL,
    name      VARCHAR(32)        NOT NULL,
    range_hr  INT                NOT NULL,
    range_3b  INT                NOT NULL,
    range_2b  INT                NOT NULL,
    range_1b  INT                NOT NULL,
    range_bb  INT                NOT NULL,
    range_fo  INT                NOT NULL,
    range_k   INT                NOT NULL,
    range_po  INT                NOT NULL,
    range_rgo INT                NOT NULL,
    range_lgo INT                NOT NULL,
    UNIQUE INDEX (shortcode),
    UNIQUE INDEX (name)
);

CREATE TABLE IF NOT EXISTS pitching_types
(
    id        INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    shortcode VARCHAR(3)         NOT NULL,
    name      VARCHAR(32)        NOT NULL,
    range_hr  INT                NOT NULL,
    range_3b  INT                NOT NULL,
    range_2b  INT                NOT NULL,
    range_1b  INT                NOT NULL,
    range_bb  INT                NOT NULL,
    range_fo  INT                NOT NULL,
    range_k   INT                NOT NULL,
    range_po  INT                NOT NULL,
    range_rgo INT                NOT NULL,
    range_lgo INT                NOT NULL,
    UNIQUE INDEX (shortcode),
    UNIQUE INDEX (name)
);

CREATE TABLE IF NOT EXISTS pitching_bonuses
(
    id        INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    shortcode VARCHAR(3)         NOT NULL,
    name      VARCHAR(32)        NOT NULL,
    range_hr  INT                NOT NULL,
    range_3b  INT                NOT NULL,
    range_2b  INT                NOT NULL,
    range_1b  INT                NOT NULL,
    range_bb  INT                NOT NULL,
    range_fo  INT                NOT NULL,
    range_k   INT                NOT NULL,
    range_po  INT                NOT NULL,
    range_rgo INT                NOT NULL,
    range_lgo INT                NOT NULL,
    UNIQUE INDEX (shortcode),
    UNIQUE INDEX (name)
);

-- Current as of session 3.14
INSERT INTO batting_types (shortcode, name, range_hr, range_3b, range_2b, range_1b, range_bb, range_fo, range_k, range_po, range_rgo, range_lgo)
VALUES ('BN', 'Basic Neutral', 11, 3, 20, 37, 20, 51, 49, 10, 24, 25),
       ('BP', 'Basic Power', 21, 1, 14, 20, 20, 60, 60, 6, 24, 24),
       ('BC', 'Basic Contact', 8, 3, 16, 52, 20, 45, 31, 10, 32, 33),
       ('MH', 'Max Homers', 40, 1, 4, 8, 6, 11, 5, 5, 85, 85),
       ('S', 'Speedy', 6, 12, 25, 20, 18, 62, 60, 28, 10, 9),
       ('XB', 'Extra Base Focus', 19, 8, 23, 10, 9, 11, 95, 25, 25, 25),
       ('1B', '1B/BB', 8, 2, 14, 41, 40, 45, 25, 1, 33, 41),
       ('EN', 'Extremely... Neutral', 15, 15, 15, 15, 15, 35, 35, 35, 35, 35),
       ('WC', 'Work The Count', 10, 4, 14, 22, 49, 40, 40, 31, 20, 20),
       ('HK', 'HR/K', 24, 1, 12, 23, 15, 31, 75, 8, 30, 31),
       ('TT', 'Three True Outcomes', 22, 2, 7, 10, 40, 20, 124, 5, 10, 10),
       ('SM', 'Sacrifice Master', 10, 2, 15, 34, 30, 99, 10, 10, 30, 10),
       ('SF', 'Single Focused', 11, 3, 15, 40, 25, 55, 50, 10, 21, 20);

INSERT INTO pitching_types (shortcode, name, range_hr, range_3b, range_2b, range_1b, range_bb, range_fo, range_k, range_po, range_rgo, range_lgo)
VALUES ('BB', 'Basic Balanced', 14, 2, 15, 36, 20, 56, 44, 12, 27, 25),
       ('BS', 'Basic Strikeout', 12, 3, 17, 35, 20, 56, 61, 7, 19, 21),
       ('BF', 'Basic Finesse', 14, 2, 15, 37, 21, 45, 33, 9, 36, 39),
       ('NH', 'No Homers', 2, 10, 25, 34, 22, 52, 40, 25, 20, 21),
       ('FP', 'Flyball Pitcher', 19, 6, 20, 12, 9, 85, 25, 25, 25, 25),
       ('TT', 'Three True Outcomes', 21, 2, 7, 10, 40, 20, 125, 5, 10, 11),
       ('TD', 'Trust Your Defense', 6, 12, 24, 28, 16, 63, 15, 14, 37, 36),
       ('EG', 'Extreme Groundballer', 10, 5, 30, 33, 14, 11, 5, 3, 70, 70),
       ('1B', '1B/BB', 8, 2, 14, 41, 40, 45, 25, 1, 34, 41),
       ('EN', 'Extremely... Neutral', 15, 15, 15, 15, 15, 35, 35, 35, 35, 36),
       ('WC', 'Weak Contact', 10, 3, 26, 30, 14, 25, 10, 110, 12, 11),
       ('NT', 'Nothing To Hit', 12, 2, 14, 25, 43, 45, 30, 8, 36, 36),
       ('SF', 'Single Focus', 10, 3, 15, 41, 25, 55, 50, 10, 21, 21),
       ('POS', 'Position', 19, 9, 19, 44, 32, 55, 10, 26, 19, 18);

INSERT INTO pitching_bonuses (shortcode, name, range_hr, range_3b, range_2b, range_1b, range_bb, range_fo, range_k, range_po, range_rgo, range_lgo)
VALUES ('H', 'Anti-Homer', -7, -1, -5, -5, -1, 2, 8, -1, 5, 5),
       ('S', 'Anti-Single', -4, -1, -7, -11, -1, 5, 8, 5, 3, 3),
       ('B', 'Balanced', -5, -1, -6, -8, -1, 4, 8, 1, 4, 4);

CREATE TABLE parks
(
    id        INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name      VARCHAR(50)        NOT NULL,
    factor_hr FLOAT              NOT NULL,
    factor_3b FLOAT              NOT NULL,
    factor_2b FLOAT              NOT NULL,
    factor_1b FLOAT              NOT NULL,
    factor_bb FLOAT              NOT NULL,
    UNIQUE INDEX (name)
);

CREATE TABLE IF NOT EXISTS teams
(
    id              INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    tag             VARCHAR(3)         NOT NULL,
    name            VARCHAR(50)        NOT NULL,
    park            INT                NOT NULL,
    color_discord   INT                NOT NULL,
    color_roster    INT                NOT NULL,
    color_roster_bg INT                NOT NULL,
    milr            BOOL               NOT NULL DEFAULT FALSE,
    milr_team       INT                NULL DEFAULT NULL,
    UNIQUE INDEX (tag),
    UNIQUE INDEX (name),
    FOREIGN KEY fk_team_park (park) REFERENCES parks (id),
    FOREIGN KEY fk_team_milr_team (milr_team) REFERENCES teams (id) ON DELETE SET NULL
);

-- Current as of session 3.14
INSERT INTO parks (name, factor_hr, factor_3b, factor_2b, factor_1b, factor_bb)
VALUES ('No Stadium', 1.000, 1.000, 1.000, 1.000, 1.000),
       ('Fake Shea Stadium', 0.963, 1.000, 1.032, 1.042, 0.879),
       ('Urban Chestnut Field', 0.969, 1.000, 0.966, 0.699, 0.763),
       ('O.Co Coliseum', 0.919, 0.950, 1.100, 0.696, 0.756),
       ('New Angel Stadium', 1.097, 0.815, 1.425, 1.209, 1.083),
       ('Petco Park', 0.973, 0.960, 0.950, 0.970, 0.972),
       ('Dodger Stadium at Chase Field', 1.090, 1.000, 1.060, 1.270, 1.040),
       ('Astroworld', 0.969, 1.000, 1.354, 1.034, 0.650),
       ('Flamingo Park', 0.998, 1.000, 0.974, 0.922, 0.946),
       ('Wrigley Field', 0.996, 1.039, 1.007, 1.199, 1.025),
       ('Beisbolcat Park', 1.000, 1.000, 1.000, 1.000, 1.000),
       ('Dodger Stadium', 0.939, 0.808, 1.003, 0.696, 1.057),
       ('The \'Foam Doam', 1.232, 1.232, 1.332, 1.513, 1.394),
       ('Kauffman Stadium', 0.840, 1.000, 1.304, 1.554, 1.212),
       ('PNC Park', 1.060, 1.000, 0.950, 0.950, 0.990),
       ('Basketball Court', 1.100, 1.030, 0.900, 0.870, 0.900),
       ('Polo Grounds', 0.808, 0.840, 0.643, 0.615, 0.650),
       ('Wawa Park at Spring Gardens', 0.972, 1.000, 0.975, 0.971, 1.080),
       ('Steve Irwin Memorial Park', 0.925, 0.925, 1.032, 0.911, 0.872),
       ('Stade Enterprise', 0.956, 0.956, 0.955, 0.879, 0.999),
       ('DiNAMO Park', 1.000, 1.000, 1.332, 1.513, 0.650);

-- MiLR teams
INSERT INTO teams (tag, name, park, color_discord, color_roster, color_roster_bg, milr)
VALUES ('BAY', 'Bayside Fish & Ships', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('BER', 'Berlin Blood Saints', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('CCF', 'Chesapeake Cangrejos Fantasmas', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('CIN', 'Cincinnati Bongo Cats', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('FTP', 'Fontana Trash Pandas', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('LAN', 'Lancaster Dumpster Fires', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('SCB', 'Schlong City Beermen', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE),
       ('TZS', 'Time Zone Spanning Jet Lag Sufferers', (SELECT parks.id FROM parks WHERE parks.name = 'The \'Foam Doam'), 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, TRUE);

-- MLR teams
INSERT INTO teams (tag, name, park, color_discord, color_roster, color_roster_bg, milr_team)
VALUES ('ARI', 'Arizona Diamondbacks', (SELECT parks.id FROM parks WHERE parks.name = 'Dodger Stadium at Chase Field'), 0x3ca7b1, 0x35d6cf, 0x7126c4, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'LAN') AS t)),
       ('BOS', 'Boston Red Sox', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0xbd3039, 0x0d2a52, 0xec114b, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BER') AS t)),
       ('CHC', 'Chicago Cubs', (SELECT parks.id FROM parks WHERE parks.name = 'Wrigley Field'), 0x1746b6, 0xcc3433, 0x0e3386, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CCF') AS t)),
       ('CLE', 'Cleveland Indians', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0x87A7EB, 0xfedc01, 0xc51e3a, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CIN') AS t)),
       ('COL', 'Colorado Rockies', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0x803fdf, 0xc3c7ca, 0x33006f, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'SCF') AS t)),
       ('DET', 'Detroit Tigers', (SELECT parks.id FROM parks WHERE parks.name = 'Beisbolcat Park'), 0xff882d, 0xffffff, 0x0c2340, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'LAN') AS t)),
       ('HOU', 'Houston Astros', (SELECT parks.id FROM parks WHERE parks.name = 'Astroworld'), 0xfffdd0, 0x072854, 0xff7f00, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BER') AS t)),
       ('KCR', 'Kansas City Royals', (SELECT parks.id FROM parks WHERE parks.name = 'Kauffman Stadium'), 0x098be5, 0xbd9b60, 0x004687, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'TZS') AS t)),
       ('LAA', 'Los Angeles Angels', (SELECT parks.id FROM parks WHERE parks.name = 'New Angel Stadium'), 0xc40404, 0xc4ced4, 0x862633, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BER') AS t)),
       ('LAD', 'Los Angeles Dodgers', (SELECT parks.id FROM parks WHERE parks.name = 'Dodger Stadium'), 0x79B5F1, 0xffffff, 0x1e90ff, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CCF') AS t)),
       ('MIA', 'Miami Marlins', (SELECT parks.id FROM parks WHERE parks.name = 'Flamingo Park'), 0xc15db4, 0xc15db4, 0x10bfe4, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BAY') AS t)),
       ('MIL', 'Milwaukee Brewers', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0xa1945e, 0xf7d600, 0x01228d, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'SCB') AS t)),
       ('MTL', 'Montréal Expos', (SELECT parks.id FROM parks WHERE parks.name = 'Stade Enterprise'), 0x86deee, 0x004c90, 0x75bcf2, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CCF') AS t)),
       ('NYM', 'New York Mets', (SELECT parks.id FROM parks WHERE parks.name = 'Fake Shea Stadium'), 0xff5910, 0xff5910, 0x002d72, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'SCB') AS t)),
       ('NYY', 'New York Yankees', (SELECT parks.id FROM parks WHERE parks.name = 'Polo Grounds'), 0xc0c0c0, 0x000000, 0xd9d9d9, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CIN') AS t)),
       ('OAK', 'Oakland Athletics', (SELECT parks.id FROM parks WHERE parks.name = 'O.Co Coliseum'), 0x00843d, 0xffcd00, 0x00843d, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'TZS') AS t)),
       ('PHI', 'Philadelphia Phillies', (SELECT parks.id FROM parks WHERE parks.name = 'Wawa Park at Spring Gardens'), 0xff1a2c, 0xba0c2e, 0xc4c2c2, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'FTP') AS t)),
       ('PIT', 'Pittsburgh Pirates', (SELECT parks.id FROM parks WHERE parks.name = 'PNC Park'), 0xffd323, 0xffc72b, 0x000000, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BAY') AS t)),
       ('SDP', 'San Diego Padres', (SELECT parks.id FROM parks WHERE parks.name = 'Petco Park'), 0x8F622B, 0xfbae0a, 0x482509, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'FTP') AS t)),
       ('SFG', 'San Francisco Giants', (SELECT parks.id FROM parks WHERE parks.name = 'Basketball Court'), 0xe13e01, 0xffffff, 0xf47836, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'BAY') AS t)),
       ('STL', 'St. Louis Cardinals', (SELECT parks.id FROM parks WHERE parks.name = 'Urban Chestnut Field'), 0xe98080, 0xc51e3a, 0xb0e0e6, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'FTP') AS t)),
       ('TBD', 'Tampa Bay Devil Rays', (SELECT parks.id FROM parks WHERE parks.name = 'Steve Irwin Memorial Park'), 0x4a8462, 0x39bd71, 0x1e297b, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'LAN') AS t)),
       ('TOR', 'Toronto Blue Jays', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0x647594, 0xeb002c, 0x0000ff, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'TZS') AS t)),
       ('TEX', 'Texas Rangers', (SELECT parks.id FROM parks WHERE parks.name = 'No Stadium'), 0x0080b4, 0xffffff, 0x003da5, (SELECT t.id FROM (SELECT teams.id FROM teams WHERE teams.tag = 'CIN') AS t));

CREATE TABLE IF NOT EXISTS users
(
    id          INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    reddit_name VARCHAR(23)        NOT NULL,
    discord     VARCHAR(40)        NULL     DEFAULT NULL,
    scopes      VARCHAR(1024)      NOT NULL DEFAULT 'player', -- CSV of allowed action scopes (player, commissioner, umpire, etc.)
    UNIQUE INDEX (reddit_name),
    UNIQUE INDEX (discord)
);

CREATE TABLE IF NOT EXISTS players
(
    id                 INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    user               INT                NOT NULL,
    name               VARCHAR(60)        NOT NULL,
    team               INT                NULL DEFAULT NULL,
    batting_type       INT                NOT NULL,
    pitching_type      INT                NULL DEFAULT NULL,
    pitching_bonus     INT                NULL DEFAULT NULL,
    rightHanded        BOOL               NOT NULL,
    position_primary   VARCHAR(2)         NOT NULL,
    position_secondary VARCHAR(2)         NULL DEFAULT NULL,
    position_tertiary  VARCHAR(2)         NULL DEFAULT NULL,
    approved           BOOL               NOT NULL DEFAULT FALSE,
    approved_by        INT                NULL DEFAULT NULL,
    approved_on        TIMESTAMP          NULL DEFAULT NULL,
    UNIQUE INDEX (name),
    FOREIGN KEY fk_player_user (user) REFERENCES users (id),
    FOREIGN KEY fk_player_team (team) REFERENCES teams (id),
    FOREIGN KEY fk_player_batting_type (batting_type) REFERENCES batting_types (id),
    FOREIGN KEY fk_player_pitching_type (pitching_type) REFERENCES pitching_types (id),
    FOREIGN KEY fk_player_pitching_bonus (pitching_bonus) REFERENCES pitching_bonuses (id),
    CHECK ( NOT approved OR (approved_by IS NOT NULL AND approved_on IS NOT NULL) )
);
