create table if not exists batting_stats
(

    player    int not null primary key,
    total_pas int not null default 0,
    total_abs int not null default 0,
    total_hr  int not null default 0,
    total_3b  int not null default 0,
    total_2b  int not null default 0,
    total_1b  int not null default 0,
    total_bb  int not null default 0,
    total_fo  int not null default 0,
    total_k   int not null default 0,
    total_po  int not null default 0,
    total_rgo int not null default 0,
    total_lgo int not null default 0,
    total_rbi int not null default 0,
    total_r   int not null default 0,
    total_sb  int not null default 0,
    total_cs  int not null default 0,
    foreign key fk_batting_stats_player (player) references players (id)

);

create table if not exists pitching_stats
(

    player     int not null primary key,
    total_pas  int not null default 0,
    total_outs int not null default 0,
    total_er   int not null default 0,
    total_hr   int not null default 0,
    total_3b   int not null default 0,
    total_2b   int not null default 0,
    total_1b   int not null default 0,
    total_bb   int not null default 0,
    total_fo   int not null default 0,
    total_k    int not null default 0,
    total_po   int not null default 0,
    total_rgo  int not null default 0,
    total_lgo  int not null default 0,
    total_sb   int not null default 0,
    total_cs   int not null default 0,
    foreign key fk_pitching_stats_player (player) references players (id)

);

insert into batting_stats(player)
select id
from players;

insert into pitching_stats(player)
select id
from players;

update batting_stats t
set total_pas=(select count(*) from game_actions where batter = t.player and play_type != 6 and play_type != 7),
    total_abs=(select count(*) from game_actions where batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != '' and result != 'BB'),
    total_rbi=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions where batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != 'BB'),
    total_hr=(select count(*) from game_actions where batter = t.player and result = 'HR'),
    total_3b=(select count(*) from game_actions where batter = t.player and result = '3B'),
    total_2b=(select count(*) from game_actions where batter = t.player and result = '2B'),
    total_1b=(select count(*) from game_actions where batter = t.player and result = '1B'),
    total_bb=(select count(*) from game_actions where batter = t.player and (result = 'BB' or result = 'Auto BB')),
    total_fo=(select count(*) from game_actions where batter = t.player and result = 'FO'),
    total_k=(select count(*) from game_actions where batter = t.player and (result = 'K' or result = 'Auto K' or result = 'Bunt K')),
    total_po=(select count(*) from game_actions where batter = t.player and result = 'PO'),
    total_rgo=(select count(*) from game_actions where batter = t.player and result = 'RGO'),
    total_lgo=(select count(*) from game_actions where batter = t.player and result = 'LGO'),
    total_sb=(select count(*) from game_actions where batter = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
    total_cs=(select count(*) from game_actions where batter = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home'));

update pitching_stats t
set total_pas=(select count(*) from game_actions where pitcher = t.player and play_type != 6 and play_type != 7),
    total_outs=(select (case when sum(outs_tracked) is null then 0 else sum(outs_tracked) end) from game_actions where pitcher = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != '' and result != 'BB'),
    total_er=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions where pitcher = t.player and play_type != 6 and play_type != 7 and play_type != 3 and result != 'BB'),
    total_hr=(select count(*) from game_actions where pitcher = t.player and result = 'HR'),
    total_3b=(select count(*) from game_actions where pitcher = t.player and result = '3B'),
    total_2b=(select count(*) from game_actions where pitcher = t.player and result = '2B'),
    total_1b=(select count(*) from game_actions where pitcher = t.player and result = '1B'),
    total_bb=(select count(*) from game_actions where pitcher = t.player and result = 'BB'),
    total_fo=(select count(*) from game_actions where pitcher = t.player and result = 'FO'),
    total_k=(select count(*) from game_actions where pitcher = t.player and result = 'K'),
    total_po=(select count(*) from game_actions where pitcher = t.player and result = 'PO'),
    total_rgo=(select count(*) from game_actions where pitcher = t.player and result = 'RGO'),
    total_lgo=(select count(*) from game_actions where pitcher = t.player and result = 'LGO'),
    total_sb=(select count(*) from game_actions where pitcher = t.player and (result = 'Steal 2B' or result = 'Steal 3B' or result = 'Steal Home' or result = 'MSteal 3B' or result = 'MSteal Home')),
    total_cs=(select count(*) from game_actions where pitcher = t.player and (result = 'CS 2B' or result = 'CS 3B' or result = 'CS Home' or result = 'CMS 3B' or result = 'CMS Home'));
