ALTER TABLE batting_stats
    ADD COLUMN total_dp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_tp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_gp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_sac INT NOT NULL DEFAULT 0;

ALTER TABLE pitching_stats
    ADD COLUMN total_dp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_tp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_gp INT NOT NULL DEFAULT 0,
    ADD COLUMN total_sac INT NOT NULL DEFAULT 0;

UPDATE batting_stats a SET
                           total_dp=(SELECT COUNT(id) FROM game_actions WHERE batter=a.player AND outs_tracked=2),
                           total_tp=(SELECT COUNT(id) FROM game_actions WHERE batter=a.player AND outs_tracked=3),
                           total_gp=(SELECT COUNT(DISTINCT lineup) FROM lineup_entries WHERE player=a.player),
                           total_sac=(SELECT COUNT(id) FROM game_actions WHERE batter=a.player AND result='FO' and runs_scored=1);

UPDATE pitching_stats a SET
                            total_dp=(SELECT COUNT(id) FROM game_actions WHERE pitcher=a.player AND outs_tracked=2),
                            total_tp=(SELECT COUNT(id) FROM game_actions WHERE pitcher=a.player AND outs_tracked=3),
                            total_gp=(SELECT COUNT(DISTINCT lineup) FROM lineup_entries WHERE player=a.player),
                            total_sac=(SELECT COUNT(id) FROM game_actions WHERE pitcher=a.player AND result='FO' and runs_scored=1);
