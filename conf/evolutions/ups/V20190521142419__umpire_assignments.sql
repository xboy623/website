CREATE TABLE IF NOT EXISTS umpire_assignments (
    game INT NOT NULL,
    umpire INT NOT NULL,
    FOREIGN KEY umpire_assignments_game (game) REFERENCES games(id),
    FOREIGN KEY umpire_assignments_umpire (umpire) REFERENCES users(id),
    PRIMARY KEY (game, umpire)
)
