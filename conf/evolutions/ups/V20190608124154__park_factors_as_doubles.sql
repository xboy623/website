alter table parks
    modify factor_hr double not null,
    modify factor_3b double not null,
    modify factor_2b double not null,
    modify factor_1b double not null,
    modify factor_bb double not null;

update parks
set factor_hr=ROUND(factor_hr, 3),
    factor_3b=ROUND(factor_3b, 3),
    factor_2b=ROUND(factor_2b, 3),
    factor_1b=ROUND(factor_1b, 3),
    factor_bb=ROUND(factor_bb, 3);
