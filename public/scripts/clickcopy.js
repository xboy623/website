// Register the on-click method to everything in the click-copy class
$('.click-copy').click(function (e) {
    // Find the object they clicked
    let target = $(e.target);
    target.select();

    // Copy the contents to the clipboard
    document.execCommand('copy');

    // Set its label to include ": COPIED!"
    let label = $('#' + target.data('copy-label'));
    let originalText = label.text();
    label.text(originalText + ': COPIED!');

    // Set the label back to original after 5s
    setTimeout(function() {
        label.text(originalText);
    }, 5000);
});
