name := "fakebaseballwebsite"

version := "1.0"

lazy val `fakebaseballwebsite` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(guice, ws)

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.12",
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "org.jsoup" % "jsoup" % "1.11.3"
)

libraryDependencies ++= Seq(
  "com.atlassian.commonmark" % "commonmark" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-autolink" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-gfm-strikethrough" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-gfm-tables" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-heading-anchor" % "0.12.1",
  "com.atlassian.commonmark" % "commonmark-ext-ins" % "0.12.1",
)


unmanagedResourceDirectories in Test += {
  baseDirectory(_ / "target/web/public/test").value
}

// Reads config file for db migration and slick-codegen
import com.typesafe.config.{Config, ConfigFactory}

lazy val configFile = settingKey[Config]("Application configuration")
configFile := ConfigFactory.load(ConfigFactory.parseFile {
  val productionConf = new File("conf/production.conf")
  if (productionConf.exists)
    productionConf
  else {
    val env = System.getenv("config.file")
    if (env == null)
      new File("conf/development.conf")
    else
      new File(env)
  }
})

// Pre-Slick DB migration
lazy val migrate = taskKey[Unit]("Applies DB Migrations")
migrate := {
  val p = new ProcessBuilder(
    "flyway",
    s"-url=${configFile.value.getString("slick.dbs.default.db.url")}",
    s"-user=${configFile.value.getString("slick.dbs.default.db.user")}",
    s"-password=${configFile.value.getString("slick.dbs.default.db.password")}",
    "-locations=filesystem:conf/evolutions/",
    "migrate"
  )
    .redirectInput(ProcessBuilder.Redirect.INHERIT)
    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
    .start()

  val errorReader = new java.io.BufferedReader(new java.io.InputStreamReader(p.getErrorStream))
  var errorMsg = ""
  new Thread(() => {
    var buf: String = ""
    while ({buf = errorReader.readLine(); buf} != null) {
      System.err.println(buf)
      errorMsg += s"$buf\n"
    }
  }).start()

  val exitCode = p.waitFor()
  if (exitCode != 0)
    throw new Exception(errorMsg)
}

// Slick DB structure code generation, runs on every compile
lazy val codegen = taskKey[Unit]("Generates Slick tables")
codegen := slick.codegen.SourceCodeGenerator.main(Array(configFile.value.getString("slick.dbs.default.profile").replace("$", ""), configFile.value.getString("slick.dbs.default.db.driver"), configFile.value.getString("slick.dbs.default.db.url"), "./app/", "model", configFile.value.getString("slick.dbs.default.db.user"), configFile.value.getString("slick.dbs.default.db.password")))
(compile in Compile) := ((compile in Compile) dependsOn codegen).value
(doc in Compile) := ((doc in Compile) dependsOn (compile in Compile)).value
codegen := (codegen dependsOn migrate).value
